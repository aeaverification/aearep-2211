clear, clc

filename=string({'EPseries_monthly_15andolder'});


[data(:,:,1), var(:,:,1)] = xlsread(filename(1),1); %all outcomes
year=data(:,1);
variables=string(var(1,:));
var(1,:)=[];
var=string(var);
month=var(:,2);
fam_status=var(:,3);
[N,M]=size(data);

E=data(:,M);
ch=data(:,4);

%% get family status
ii_smwoc=find(and(fam_status=={'Single Men'},ch==0));
ii_mmwoc=find(and(fam_status=={'Married Men'},ch==0));
ii_smwc=find(and(fam_status=={'Single Men'},ch==1));
ii_mmwc=find(and(fam_status=={'Married Men'},ch==1));

ii_sfwoc=find(and(fam_status=={'Single Women'},ch==0));
ii_mfwoc=find(and(fam_status=={'Married Women'},ch==0));
ii_sfwc=find(and(fam_status=={'Single Women'},ch==1));
ii_mfwc=find(and(fam_status=={'Married Women'},ch==1));

e_smwoc=E(ii_smwoc); e_mmwoc=E(ii_mmwoc); e_smwc=E(ii_smwc); e_mmwc=E(ii_mmwc);
e_m=[e_smwoc,e_smwc,e_mmwoc,e_mmwc];

e_sfwoc=E(ii_sfwoc); e_mfwoc=E(ii_mfwoc); e_sfwc=E(ii_sfwc); e_mfwc=E(ii_mfwc);
e_f=[e_sfwoc,e_sfwc, e_mfwoc, e_mfwc];

N=length(e_smwoc);
year=year(1:N);
month=month(1:N);


%% get recessions and expansion dates
%GR
ii_GR_pre_1=find(and(year==2007,month=={'March'}));
ii_GR_pre_2=find(and(year==2007,month=={'October'}));
ii_GR_end=find(and(year==2009,month=={'June'}));
ii_GR_rec=find(and(year==2012,month=={'July'}));

ii_GR_pre=ii_GR_pre_1:1:ii_GR_pre_2;
ii_GR_recession=ii_GR_pre_2+1:1:ii_GR_end;
ii_GR_recovery=ii_GR_end+1:1:ii_GR_rec;

e_m_recession=mean(e_m(ii_GR_recession,:),1)./mean(e_m(ii_GR_pre,:),1);
e_m_recovery=mean(e_m(ii_GR_recovery,:),1)./mean(e_m(ii_GR_pre,:),1);
e_f_recession=mean(e_f(ii_GR_recession,:),1)./mean(e_f(ii_GR_pre,:),1);
e_f_recovery=mean(e_f(ii_GR_recovery,:),1)./mean(e_f(ii_GR_pre,:),1);

%Pandemic Recession
ii_PR_pre=find(and(year==2020,month=={'February'}));
ii_PR_1_end=find(and(year==2020,month=={'May'}));
ii_PR_2_end=find(and(year==2020,month=={'December'}));

ii_PR_1=ii_PR_pre+1:1:ii_PR_1_end;
ii_PR_2=ii_PR_1_end+1:1:ii_PR_2_end;

e_m_1=mean(e_m(ii_PR_1,:),1)./mean(e_m(ii_PR_pre,:),1);
e_m_2=mean(e_m(ii_PR_2,:),1)./mean(e_m(ii_PR_pre,:),1);
e_f_1=mean(e_f(ii_PR_1,:),1)./mean(e_f(ii_PR_pre,:),1);
e_f_2=mean(e_f(ii_PR_2,:),1)./mean(e_f(ii_PR_pre,:),1);

delta_e(:,:,1)=100*([e_m_recession', e_f_recession';e_m_recovery', e_f_recovery']-1);
delta_e(:,:,2)=100*([e_m_1', e_f_1';e_m_2', e_f_2']-1);

cycle=["GR","Covid"];
    
for ff=1:2
    
    %% get data for bar charts
    bar_m_1=delta_e(1:4,1,ff); bar_f_1=delta_e(1:4,2,ff);
    bar_m_2=delta_e(5:8,1,ff); bar_f_2=delta_e(5:8,2,ff);
    
    
    figure(ff)
    ylim_low=-20; ylim_high=0.5;
    ylim([ylim_low ylim_high]);
  
    y_f=[bar_f_1,bar_f_2]';
    y_m=[bar_m_1,bar_m_2]';
    b_m=bar([1,2],y_m,0.8,'FaceColor','r','FaceAlpha',0.5,'LineWidth',1.5);
    %b_m.FaceColor=[0.5 0.5 0.5];
    txt = '\leftarrow Men';
    [t_y,t_x]=min(y_m(1,:));
    t_m=text(1-0.5+0.25*t_x,t_y,txt,'FontSize',14,'FontWeight','bold');
    hold on
    b_f=bar([1,2],y_f,0.4,'FaceColor','w','FaceAlpha',1,'LineWidth',1.5);
    txt = '\leftarrow Women';
    [t_y,t_x]=min(y_f(1,:));
    t_f=text(1-0.5+0.25*t_x,t_y,txt,'FontSize',14,'FontWeight','bold');
    hold off
    title('Employment to Population Ratio');
    
    
    ylim([ylim_low ylim_high]);
    
    
    
    file_xticks={'Phase 1','Phase 2'};
    if ff==1
        file_xticks={'Recession','Recovery'};
    end
    xticklabels(file_xticks);
    ylabel('percentage points');
    ax=gca; ax.FontSize=12;
    
    lgd=legend('Single w/o children','Single with children','Married w/o children','Married with children');
    lgd.Box=('off'); lgd.FontSize=14; lgd.Location=('Best');
    
    name="EoverP_barchart_overlay_";
    name=name+cycle(ff);
    pic_name=name;
    saveas(gcf,pic_name,'png')
    %savefig(pic_name)
    
end




