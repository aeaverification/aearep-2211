clear, clc

filename='EPseries_monthly_sa_all.xlsx';
sheet=1;

[ep_data,ep_var]=xlsread(filename,sheet,'A2:P1082');

year=ep_data(:,1);
ep_data(:,1:2)=[];
ep_var_row=ep_var(1,:);
month=ep_var(:,2);
group=ep_var(:,3);
month(1)=[]; group(1)=[];
month=string(month);
group=string(group);

[n,m]=size(ep_data);


ii_f=find(group=='Female');
ii_m=find(group=='Male');
ep=[ep_data(ii_m,m),ep_data(ii_f,m)];

[N,M]=size(ep);
year=year(1:N);

%% calculate aggregate E/P
w_f=ep_data(ii_f,m-3)./(ep_data(ii_f,m-3)+ep_data(ii_m,m-3));
w_m=1-w_f;
ep(:,3)=ep(:,1).*w_m+ep(:,2).*w_f;


filename='NBER_recessions.xlsx';
sheet=1;
[rec_data,rec_var]=xlsread(filename,sheet);
year_rec=rec_data(:,1); rec_data(:,1)=[];
rec_dates=rec_data(:,2);
UR=rec_data(:,3);
rec_ind=rec_data(:,4);

%% select recessions starting in 1990
one=find(rec_ind==1); one_rec=max(one)+1:max(one)+36; one=[one; one_rec' ];
two=find(rec_ind==2); two_rec=max(two)+1:max(two)+36; two=[two; two_rec' ];
three=find(rec_ind==3); three_rec=max(three)+1:max(three)+36; three=[three; three_rec' ];
four=find(rec_ind==4); %four_rec=max(four)+1:N; four=[four; four_rec' ];


%% 6 month MA untill 2020
for tt=4:1:N-12
    ep(tt,:)=mean(ep(tt-3:tt+3,:),1);
end

ep_1=ep(one,:); n_1=size(ep_1,1);
ep_2=ep(two,:); n_2=size(ep_2,1);
ep_3=ep(three,:); n_3=size(ep_3,1);
ep_4=ep(four,:); n_4=size(ep_4,1);

d_ep_1=100*(ep_1./ep_1(1,:)-1);
d_ep_2=100*(ep_2./ep_2(1,:)-1);
d_ep_3=100*(ep_3./ep_3(1,:)-1);
d_ep_4=100*(ep_4./ep_4(1,:)-1);

time=0:1:max([n_1,n_2,n_3]);


figure(1);
plot(time(1:n_1),d_ep_1(:,1),'-.b',time(1:n_1),d_ep_1(:,2),'--r',time(1:n_1),d_ep_1(:,3),':k','LineWidth',4)
ax = gca;
ax.FontSize=14;
lgd=legend('Men','Women','Aggregate','Location','best'); lgd.Box=('off'); lgd.FontSize=14;
xticks=(time(1:n_1));
axis tight; ylim([-5 2])
xlabel('months since recession start'), ylabel('percent change')
title('1990-1991 cycle')
saveas(gcf,'event_cycle_1.png')


figure(2);
plot(time(1:n_2),d_ep_2(:,1),'-.b',time(1:n_2),d_ep_2(:,2),'--r',time(1:n_2),d_ep_2(:,3),':k','LineWidth',4)
ax = gca;
ax.FontSize=14;
lgd=legend('Men','Women','Aggregate','Location','best'); lgd.Box=('off'); lgd.FontSize=14;
xticks=(time(1:n_2));
axis tight; ylim([-5 2])
xlabel('months since recession start'), ylabel('percent change')
title('2001 cycle')
saveas(gcf,'event_cycle_2.png')

figure(3);
plot(time(1:n_3),d_ep_3(:,1),'-.b',time(1:n_3),d_ep_3(:,2),'--r',time(1:n_3),d_ep_3(:,3),':k','LineWidth',4)
ax = gca;
ax.FontSize=14;
lgd=legend('Men','Women','Aggregate','Location','best'); lgd.Box=('off'); lgd.FontSize=14;
xticks=(time(1:n_3));
axis tight; ylim([-10 2])
xlabel('months since recession start'), ylabel('percent change')
title('2007-2009 cycle')
saveas(gcf,'event_cycle_3.png')

figure(4);
plot(time(1:n_4),d_ep_4(:,1),'-.b',time(1:n_4),d_ep_4(:,2),'--r',time(1:n_4),d_ep_4(:,3),':k','LineWidth',4)
ax = gca;
ax.FontSize=14;
lgd=legend('Men','Women','Aggregate','Location','best'); lgd.Box=('off'); lgd.FontSize=14;
xticks=(time(1:n_4));
axis tight; ylim([-25 2])
xlabel('months since recession start'), ylabel('percent change')
title('2020 cycle')
saveas(gcf,'event_cycle_4.png')

