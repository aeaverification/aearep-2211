%real time analysis 
clear, clc

filename='CPS_COVID_EMPLOYMENT_NEW.xlsx';

%% by occupation
sheet=3;

[data,var]=xlsread(filename,sheet);
year=data(:,1);
var_row=var(1,:); var_row=string(var_row);
month=var(:,2); occgroup=var(:,3); 
month=string(month); month(1)=[];
occgroup=string(occgroup); occgroup(1)=[];

[N,M]=size(data);
N=N/4;
% lfp=1-data(:,M); 
% data(:,M)=lfp;

log_data=log(data);
data=log_data;

ii_flex_high=find(occgroup=='Flexible, High-Contact');
ii_inflex_high=find(occgroup=='Inflexible, High-Contact');
ii_flex_low=find(occgroup=='Flexible, Low-Contact');
ii_inflex_low=find(occgroup=='Inflexible, Low-Contact');


occ_flex_high=data(ii_flex_high,:);
occ_flex_low=data(ii_flex_low,:);
occ_inflex_high=data(ii_inflex_high,:);
occ_inflex_low=data(ii_inflex_low,:);

%log variation from 1 year prior
logv_FH=occ_flex_high(13:N,:)-occ_flex_high(1:11,:); logv_FH=100*logv_FH;
logv_FL=occ_flex_low(13:N,:)-occ_flex_low(1:11,:); logv_FL=100*logv_FL;
logv_IH=occ_inflex_high(13:N,:)-occ_inflex_high(1:11,:); logv_IH=100*logv_IH;
logv_IL=occ_inflex_low(13:N,:)-occ_inflex_low(1:11,:); logv_IL=100*logv_IL;


%% by gender and occupation
sheet='GenderOccupation';

[data,var]=xlsread(filename,sheet);
year=data(:,1);
var_row=var(1,:); var_row=string(var_row);
month=var(:,2); sex=var(:,3);occgroup=var(:,4); 
month=string(month); month(1)=[];
sex=string(sex); sex(1)=[];
occgroup=string(occgroup); occgroup(1)=[];

[N,M]=size(data);
N=N/8;
% lfp=1-data(:,M); 
% data(:,M)=lfp;

log_data=log(data);
data=log_data;

ii_flex_high_m=find(and(sex=='Male',occgroup=='Flexible, High-Contact'));
ii_flex_high_f=find(and(sex=='Female',occgroup=='Flexible, High-Contact'));

ii_inflex_high_m=find(and(sex=='Male',occgroup=='Inflexible, High-Contact'));
ii_inflex_high_f=find(and(sex=='Female',occgroup=='Inflexible, High-Contact'));

ii_flex_low_m=find(and(sex=='Male',occgroup=='Flexible, Low-Contact'));
ii_flex_low_f=find(and(sex=='Female',occgroup=='Flexible, Low-Contact'));

ii_inflex_low_m=find(and(sex=='Male',occgroup=='Inflexible, Low-Contact'));
ii_inflex_low_f=find(and(sex=='Female',occgroup=='Inflexible, Low-Contact'));


occ_flex_high_m=data(ii_flex_high_m,:);
occ_flex_low_m=data(ii_flex_low_m,:);
occ_inflex_high_m=data(ii_inflex_high_m,:);
occ_inflex_low_m=data(ii_inflex_low_m,:);

occ_flex_high_f=data(ii_flex_high_f,:);
occ_flex_low_f=data(ii_flex_low_f,:);
occ_inflex_high_f=data(ii_inflex_high_f,:);
occ_inflex_low_f=data(ii_inflex_low_f,:);

%log variation from 1 year prior
logv_FH_m=occ_flex_high_m(13:N,:)-occ_flex_high_m(1:11,:); logv_FH_m=100*logv_FH_m;
logv_FL_m=occ_flex_low_m(13:N,:)-occ_flex_low_m(1:11,:); logv_FL_m=100*logv_FL_m;
logv_IH_m=occ_inflex_high_m(13:N,:)-occ_inflex_high_m(1:11,:); logv_IH_m=100*logv_IH_m;
logv_IL_m=occ_inflex_low_m(13:N,:)-occ_inflex_low_m(1:11,:); logv_IL_m=100*logv_IL_m;

logv_FH_f=occ_flex_high_f(13:N,:)-occ_flex_high_f(1:11,:); logv_FH_f=100*logv_FH_f;
logv_FL_f=occ_flex_low_f(13:N,:)-occ_flex_low_f(1:11,:); logv_FL_f=100*logv_FL_f;
logv_IH_f=occ_inflex_high_f(13:N,:)-occ_inflex_high_f(1:11,:); logv_IH_f=100*logv_IH_f;
logv_IL_f=occ_inflex_low_f(13:N,:)-occ_inflex_low_f(1:11,:); logv_IL_f=100*logv_IL_f;



fig_2_xticks={'January','February','March','April','May', 'June','July','August','September','October','November'};
xx=categorical(fig_2_xticks);
xx=[1 2 3 4 5 6 7 8 9 10 11];
xy_lim=[2 11 -50 10; 2 11 -35 45; 2 11 -175 200 ];

figure(81);
for ii=10
    subplot(2, 2, 1); plot(xx,logv_FH_m(:,ii),'-.b',xx,logv_FH_f(:,ii),'--r',xx,logv_FH(:,9),':k','linewidth',4);
    lgd=legend('Men','Women','Aggregate','Location','best'); lgd.Box=('off'); lgd.FontSize=14;
    ylabel('percent'); xticks(xx); xticklabels(fig_2_xticks); xtickangle(45); axis(xy_lim(ii-9,:));
    ax=gca; ax.FontSize=14;
    title('Flex/High contact','Fontsize',14)
    subplot(222); plot(xx,logv_FL_m(:,ii),'-.b',xx,logv_FL_f(:,ii),'--r',xx,logv_FL(:,9),':k','linewidth',4);
    lgd=legend('Men','Women','Aggregate','Location','best'); lgd.Box=('off'); lgd.FontSize=14;
    ylabel('percent'); xticks(xx); xticklabels(fig_2_xticks); xtickangle(45); axis(xy_lim(ii-9,:));
    ax=gca; ax.FontSize=14;
    title('Flex/Low contact','Fontsize',14)
    subplot(223); plot(xx,logv_IH_m(:,ii),'-.b',xx,logv_IH_f(:,ii),'--r',xx,logv_IH(:,9),':k','linewidth',4);
    lgd=legend('Men','Women','Aggregate','Location','best'); lgd.Box=('off'); lgd.FontSize=14;
    ylabel('percent');xticks(xx); xticklabels(fig_2_xticks); xtickangle(45); axis(xy_lim(ii-9,:));
    ax=gca; ax.FontSize=14;
    title('Inflex/High contact','Fontsize',14)
    subplot(224); plot(xx,logv_IL_m(:,ii),'-.b',xx,logv_IL_f(:,ii),'--r',xx,logv_IL(:,9),':k','linewidth',4);
    lgd=legend('Men','Women','Aggregate','Location','best'); lgd.Box=('off'); lgd.FontSize=14;
    ylabel('percent'); xticks(xx); xticklabels(fig_2_xticks); xtickangle(45); axis(xy_lim(ii-9,:));
    ax=gca; ax.FontSize=14;
    title('Inflex/Low contact','Fontsize',14)
end

saveas(gcf,'ep_by_occgender_agg_yoy','png')



