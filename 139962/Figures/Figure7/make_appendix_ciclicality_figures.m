%cyclicality by occupation and marital status based on yearly data
clc, clear

%% HP filter smoothing parameter
lambda=6.5;

%% GDP data
filename='GDP.xlsx';
sheet=1;
[gdp_data,gdp_var]=xlsread(filename,sheet);
gdp=log(gdp_data(:,2));
year=gdp_data(:,1);
[gdpT,gdpC]=hpfilter(gdp,lambda);
gdpC_std=std(gdpC);
ll=1; %correlation leads and lags

%% Labor market data
filename='series_by_occ_maritalst.xlsx';
sheet=1;
%variables={'year',	'ryear',	'occgroup',	'emprate_marriedmen',	'emprate_singlemen',	'emprate_marriedwomen',	'emprate_singlewomen','unemprate_marriedmen','unemprate_singlemen','unemprate_marriedwomen','unemprate_singlewomen','hours_marriedmen_yearly','	hours_singlemen_yearly','hours_marriedwomen_yearly','hours_singlewomen_yearly','hours_marriedmen_weekly','hours_singlemen_weekly','hours_marriedwomen_weekly','hours_singlewomen_weekly'};

[occ_data,occ_var]=xlsread(filename,sheet);
%year=occ_data(:,2);
occ_var_row=occ_var(1,:);
occgroup=occ_var(:,3);
occgroup(1)=[];
occgroup=string(occgroup);
occ_data(:,1:3)=[];
occ_data=log(occ_data);
[N,M]=size(occ_data);

ii_flex_high=find(occgroup=='Flexible & High');
ii_inflex_high=find(occgroup=='Inflexible & High');
ii_flex_low=find(occgroup=='Flexible & Low');
ii_inflex_low=find(occgroup=='Inflexible & Low');

[occT_flex_high,occC_flex_high]=hpfilter(occ_data(ii_flex_high,:),lambda);
[occT_inflex_high,occC_inflex_high]=hpfilter(occ_data(ii_inflex_high,:),lambda);
[occT_flex_low,occC_flex_low]=hpfilter(occ_data(ii_flex_low,:),lambda);
[occT_inflex_low,occC_inflex_low]=hpfilter(occ_data(ii_inflex_low,:),lambda);

std_flex_high=std(occC_flex_high)/gdpC_std;
std_inflex_high=std(occC_inflex_high)/gdpC_std;
std_flex_low=std(occC_flex_low)/gdpC_std;
std_inflex_low=std(occC_inflex_low)/gdpC_std;

ll=1;
for jj=1:M
    
    cc=crosscorr(occC_flex_high(:,jj),gdpC,ll);
    corr_flex_high(:,jj)=cc';
    
    cc=crosscorr(occC_inflex_high(:,jj),gdpC,ll);
    corr_inflex_high(:,jj)=cc';
    
    cc=crosscorr(occC_flex_low(:,jj),gdpC,ll);
    corr_flex_low(:,jj)=cc';
    
    cc=crosscorr(occC_inflex_low(:,jj),gdpC,ll);
    corr_inflex_low(:,jj)=cc';
    
end



%% Make figures
figure(1),
ff=[std_flex_high(1:4)', std_flex_low(1:4)', std_inflex_high(1:4)', std_inflex_low(1:4)']';
fig_1_xticks={'Flex/High contact','Flex/Low contact','Inflex/High contact','Inflex/Low contact'};
xx=categorical(fig_1_xticks);
bar(xx,ff); colormap(cool);
lgd=legend('Married men','Single men','Married women','Single women','Location','best'); lgd.Box=('off'); lgd.FontSize=14;
ylabel('Standard deviation relative to GDP')
ax=gca; ax.FontSize=14;
title('E/P','Fontsize',14)
saveas(gcf,'e_stdev_byocc','png')


figure(3);
ff=[corr_flex_high(ll+1,1:4)', corr_flex_low(ll+1,1:4)', corr_inflex_high(ll+1,1:4)', corr_inflex_low(ll+1,1:4)']';
fig_1_xticks={'Flex/High contact','Flex/Low contact','Inflex/High contact','Inflex/Low contact'};
xx=categorical(fig_1_xticks);
bar(xx,ff); colormap(cool);
lgd=legend('Married men','Single men','Married women','Single women','Location','best'); lgd.Box=('off'); lgd.FontSize=14;
ylabel('Correlation with GDP_{t-1}')
ax=gca; ax.FontSize=14;
title('E/P','Fontsize',14)
saveas(gcf,'e_corr_0_byocc','png')



