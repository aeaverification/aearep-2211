%% This script generates the charts for the regression results included in the May version of the draft
clear, clc

%% import population distributions
[pop_data,pop_var]=xlsread('pop_distr.xlsx',1,'B3:C11');
pop_data=pop_data/100;
pop_2020=pop_data(:,1); pop_2007=pop_data(:,2);


%% import regression results
filename=string({'Corona_Noocc', 'Standard_Noocc', 'Corona_2digitocc','Standard_2digitocc'});

[file_data(:,:,1), file_var(:,:,1)] = xlsread(filename(1),1,'B80:M103'); %all outcomes
[err_data(:,:,1) , err_var(:,:,1) ] = xlsread(filename(1),1,'B104:M127'); %data for standard errors
[file_data(:,:,2), file_var(:,:,2)] = xlsread(filename(2),1,'B80:M103');
[err_data(:,:,2) , err_var(:,:,2) ] = xlsread(filename(2),1,'B104:M127');
[file_data(:,:,3), file_var(:,:,3)] = xlsread(filename(3),1,'B206:M229');
[err_data(:,:,3) , err_var(:,:,3) ] = xlsread(filename(3),1,'B230:M253');
[file_data(:,:,4), file_var(:,:,4)] = xlsread(filename(4),1,'B206:M229');
[err_data(:,:,4) , err_var(:,:,4) ] = xlsread(filename(4),1,'B230:M253');

[xx,effects] = xlsread(filename(1),1,'A80:A103'); %coefficients and gaps
effects=string(effects); xx=[];

[xx,variables]= xlsread(filename(1),1,'B3:M3'); %coefficients and gaps
variables=string(variables); xx=[];
titles=variables;
titles(10)="Employment";
titles(12)="Non-participation";
titles(11)="Unemployment";
titles(1)='Employment-to-nonparticipation flow';
titles(2)='Unemployment-to-nonparticipation flow';
titles(3)='Employment-to-unemployment flow';
titles(5)='Unemployment-to-employment flow';

for nn=[1,2,3,5,10,11,12]
    %% calculate aggreate changes, no occupation controls
    E_agg_covid = [ pop_2020'*file_data(1:2:16,nn,1) pop_2020'*file_data(2:2:16,nn,1)];
    E_agg_GR = [ pop_2007'*file_data(1:2:16,nn,2) pop_2007'*file_data(2:2:16,nn,2)];
    
    E_sharef_covid = [ pop_2020(5:8)'*file_data(9:2:16,nn,1) pop_2020(5:8)'*file_data(10:2:16,nn,1)]./E_agg_covid;
    E_sharef_GR = [ pop_2007(5:8)'*file_data(9:2:16,nn,2) pop_2007(5:8)'*file_data(10:2:16,nn,2)]./ E_agg_GR;
    
    %% calculate aggreate changes, with occupation controls
    E_agg_covid_occ = [ pop_2020'*file_data(1:2:16,nn,3) pop_2020'*file_data(2:2:16,nn,3)];
    E_agg_GR_occ = [ pop_2007'*file_data(1:2:16,nn,4) pop_2007'*file_data(2:2:16,nn,4)];
    
    E_sharef_covid_occ = [ pop_2020(5:8)'*file_data(9:2:16,nn,3) pop_2020(5:8)'*file_data(10:2:16,nn,3)]./E_agg_covid_occ;
    E_sharef_GR_occ = [ pop_2007(5:8)'*file_data(9:2:16,nn,4) pop_2007(5:8)'*file_data(10:2:16,nn,4)]./ E_agg_GR_occ;
    
    %% Table
    variables(nn)
    T_data=[E_agg_covid, E_agg_GR; E_sharef_covid, E_sharef_GR]*100;
    T=table(T_data(:,1), T_data(:,2), T_data(:,3), T_data(:,4),'RowNames',{'change, w/o occupation controls','Share female'},'VariableNames',{'Phase1','Phase2','Recession','Recovery'})
    
    T_data_occ=[E_agg_covid_occ, E_agg_GR_occ; E_sharef_covid_occ, E_sharef_GR_occ]*100;
    T_occ=table(T_data_occ(:,1), T_data_occ(:,2), T_data_occ(:,3), T_data_occ(:,4),'RowNames',{'change, with occupation controls','Share female'},'VariableNames',{'Phase1','Phase2','Recession','Recovery'})
    
end


sz = size(file_data);

data = file_data*100;
err=err_data*100;


for ff = 1:sz(3)
    
    for nn=[1,2,3,5,10,11,12]
        bar_m_1=data(1:2:8,nn,ff); bar_f_1=data(9:2:16,nn,ff); bar_gap_1=data(17:2:24,nn,ff);
        bar_m_2=data(2:2:8,nn,ff); bar_f_2=data(10:2:16,nn,ff); bar_gap_2=data(18:2:24,nn,ff);
        
        err_m_1=err(1:2:8,nn,ff); err_f_1=err(9:2:16,nn,ff); err_gap_1=err(17:2:24,nn,ff);
        err_m_2=err(2:2:8,nn,ff); err_f_2=err(10:2:16,nn,ff); err_gap_2=err(18:2:24,nn,ff);
        
        
        figure(ff*1000+nn),
        %bar([bar_m_1, bar_m_2;bar_f_1,bar_f_2]);
        y=[bar_gap_1,bar_gap_2]';
        se=[err_gap_1,err_gap_2]';
        bar([1,2],y,0.8)
        title(titles(nn));
        ylabel('female-male, percentage points');
        
        ylim_low=min(min(data(17:24,nn,ff)-err(17:24,nn,ff)))-0.1;
        ylim_high=max(max(data(17:24,nn,ff)+err(17:24,nn,ff)))+0.1;
        if and(nn==10,or(ff==1,ff==3))
            ylim_low=-5; ylim_high=0.5;
        elseif and(nn==12,or(ff==1,ff==3))
            ylim_low=-0.5; ylim_high=1.5;
        elseif and(nn==10,or(ff==2,ff==4))
            ylim_low=-0.5; ylim_high=4;
        elseif and(nn==11,or(ff==2,ff==4))
            ylim_low=-4; ylim_high=1;
        elseif and(nn==3,or(ff==1,ff==3))
            ylim_low=-0.5; ylim_high=3.5;
        elseif and(nn==11,or(ff==1,ff==3))
            ylim_low=-1.5; ylim_high=5;
        end
        
        ylim([ylim_low ylim_high]);
        
        file_xticks={'Phase 1','Phase 2'};
        if or(ff==2,ff==4)
            file_xticks={'Recession','Recovery'};
        end
        xticklabels(file_xticks);
        
        ax=gca; ax.FontSize=12;
        
        hold on
        ngroups = size(y, 1);
        nbars = size(y, 2);
        % Calculating the width for each bar group
        groupwidth = min(0.8, nbars/(nbars + 1.5));
        for i = 1:nbars
            x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
            errorbar(x, y(:,i), se(:,i),'k.','CapSize',16,'LineWidth',1.5');
            %     er.Color=[0 0 0];
            %     er.LineStyle='none';
        end
        
        lgd=legend('Single w/o children','Single with children','Married w/o children','Married with children');
        lgd.Box=('off'); lgd.FontSize=14; lgd.Location=('Best');
        hold off
        
        name="_barchart_gap_";
        name=name+filename(ff);
        var=variables(nn);
        pic_name=var+name;
        saveas(gcf,pic_name,'png')
        %savefig(pic_name)
    end
    
    
end






