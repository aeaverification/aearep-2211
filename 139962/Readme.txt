

Project ID: openicpsr-139962

*CAUTION* TABLE AND FIGURE NUMBERS HERE ARE NOT DEFINITIVE. THEY MAY CHANGE LATER.


To replicate the results in the paper, run "master.do" then run the matlab codes in the "Figure" folder to create figures.

Description

1. Raw data:

-"cps_00073.dta": Basic monthly CPS, Jan 2019 - Nov 2020. One needs this dataset to obtain the regression results for the COVID-19 recession.

-"cps_00074.dta": Basic monthly CPS, Jan 2006 - Dec 2020. One needs this dataset to obtain the regression results for the Great recession.

-"cps_00075.dta": Basic monthly CPS, Jan 1976 - Dec 2020. This is to create the long time series for E/P ratios.

-"Work Activities.txt" and "Work Context.txt": these are from the ONET. One needs them to classify occupations as the paper does.

-"occlist.dta": It links the ONET occupation codes with occupation names

-"crosswalk.dta": occpation code crosswalk between the ONET and the CPS

-"occ1990dd_task_alm": It contains abstract, routine, and manual task indexes for each occupation, obtained from David Dorn's webpage.

-"occ2010_occ1990.dta": crosswalk between the 1990 and 2010 Census Bureau occupational classification schemes.






2. Dofiles 

-"master.do" executes all the necessary dofiles in order.

-"regressions_covid19.do" generates the regression results for the COVID-19 recession.

-"regression_great_recession.do" generates the regression results for the Great recession.

-"ONET_analysis.do" generates the flexibility and contact intensity scores for each occupation and classifies occupations into four big categories.

-"generate_EPseries.do" generates employment-population ratios by gender, marital status, the presence of children, and occupation.

-The rest of the dofiles are to do data preparation for the regression analyses.
 




 
3. Results

-Once "master.do" finishes running, one can find twelve excel files in the "Results" folder.


-"EPseries_monthly_sa_all.xlsx" is needed to run "make_figure_1.m". MAKE SURE to get seasonality adjusted for before running the matlab code. The Eviews workfile for seasonal adjustment is in the same folder as "make_figure_1.m"

-"EPseries_monthly_15andolder.xlsx" is needed to run "make_figure_2.m"

-"CPS_COVID_EMPLOYMENT_NEW.xlsx" is needed to run "make_figure_3.m"

-"Corona_2digitocc.xml", "Corona_Noocc.xml", "Standard_2digit.xml", "Standard_Noocc.xml" and "pop_distr.xlsx" are needed to run "make_regression_charts.m" which creates Figure 4-6, Figure 8-9. 
PLEASE conver the .xml files to .xlsx format before running the matlab code.
"make_regression_charts.m" also creates Table 3-4 and Table 8.

-"classification.xlsx" creates Table 1.

-"occ_distribution.xlsx" creates Table 2 and Table 5 (See the last column for Table 5).

-"occ_by_flex_cont.xlsx" creates Table 6.

-"occ_distribution_gc.xlsx" creates Table 7.

-"share_demogroup_covid.xlsx" and "share_demogroup_gr.xlsx" create Table 9.




