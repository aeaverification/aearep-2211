


use "${raw}/occ2010_occ1990", clear
rename occ1990 occ1990dd
merge m:1 occ1990dd using "${raw}/occ1990dd_task_alm" 

keep if _merge == 3


gen management = 1 if occ2010 >= 10 & occ2010 <= 430
replace management = 0 if occ2010 < 10 | occ2010 > 430

gen business = 1 if occ2010 >= 500 & occ2010 <= 950
replace business = 0 if occ2010 < 500 | occ2010 > 950

gen compmath = 1 if occ2010 >= 1000 & occ2010 <= 1240
replace compmath = 0 if occ2010 < 1000 | occ2010 > 1240

gen engineer = 1 if occ2010 >= 1300 & occ2010 <= 1560
replace engineer = 0 if occ2010 < 1300 | occ2010 > 1560

gen research = 1 if occ2010 >= 1600 & occ2010 <= 1980
replace research = 0 if occ2010 < 1600 | occ2010 > 1980

gen socialservice = 1 if occ2010 >= 2000 & occ2010 <= 2060
replace socialservice = 0 if occ2010 < 2000 | occ2010 > 2060

gen legal = 1 if occ2010 >= 2100 & occ2010 <= 2160
replace legal = 0 if occ2010 < 2100 | occ2010 > 2160

gen education = 1 if occ2010 >= 2200 & occ2010 <= 2550
replace education = 0 if occ2010 < 2200 | occ2010 > 2550

gen arts = 1 if occ2010 >= 2600 & occ2010 <= 2920
replace arts = 0 if occ2010 < 2600 | occ2010 > 2920

gen healthtech = 1 if occ2010 >= 3000 & occ2010 <= 3540
replace healthtech = 0 if occ2010 < 3000 | occ2010 > 3540

gen healthsupport = 1 if occ2010 >= 3600 & occ2010 <= 3655
replace healthsupport = 0 if occ2010 < 3600 | occ2010 > 3655

gen protective = 1 if occ2010 >= 3700 & occ2010 <= 3955
replace protective = 0 if occ2010 < 3700 | occ2010 > 3955

gen foodprep = 1 if occ2010 >= 4000 & occ2010 <= 4150
replace foodprep = 0 if occ2010 < 4000 | occ2010 > 4150

gen cleaning = 1 if occ2010 >= 4200 & occ2010 <= 4250
replace cleaning = 0 if occ2010 < 4200 | occ2010 > 4250

gen personalcare = 1 if occ2010 >= 4300 & occ2010 <= 4650
replace personalcare = 0 if occ2010 < 4300 | occ2010 > 4650

gen sales = 1 if occ2010 >= 4700 & occ2010 <= 4965
replace sales = 0 if occ2010 < 4700 | occ2010 > 4965

gen office = 1 if occ2010 >= 5000 & occ2010 <= 5940
replace office = 0 if occ2010 < 5000 | occ2010 > 5940

gen farm = 1 if occ2010 >= 6005 & occ2010 <= 6130
replace farm = 0 if occ2010 < 6005 | occ2010 > 6130

gen construction = 1 if occ2010 >= 6200 & occ2010 <= 6940
replace construction = 0 if occ2010 < 6200 | occ2010 > 6940

gen installation = 1 if occ2010 >= 7000 & occ2010 <= 7630
replace installation = 0 if occ2010 < 7000 | occ2010 > 7630

gen production = 1 if occ2010 >= 7700 & occ2010 <= 8965
replace production = 0 if occ2010 < 7700 | occ2010 > 8965

gen transportation = 1 if occ2010 >= 9000 & occ2010 <= 9750
replace transportation = 0 if occ2010 < 9000 | occ2010 > 9750

gen military = 1 if occ2010 == 9830	
replace military = 0 if occ2010 != 9830



gen soccode = .
replace soccode = 11 if management == 1
replace soccode = 13 if business == 1
replace soccode = 15 if compmath == 1
replace soccode = 17 if engineer == 1
replace soccode = 19 if research == 1
replace soccode = 21 if socialservice == 1
replace soccode = 23 if legal == 1
replace soccode = 25 if education == 1
replace soccode = 27 if arts == 1
replace soccode = 29 if healthtech == 1
replace soccode = 31 if healthsupport == 1
replace soccode = 33 if protective == 1
replace soccode = 35 if foodprep == 1
replace soccode = 37 if cleaning == 1
replace soccode = 39 if personalcare == 1
replace soccode = 41 if sales == 1
replace soccode = 43 if office == 1
replace soccode = 45 if farm == 1
replace soccode = 47 if construction == 1
replace soccode = 49 if installation == 1
replace soccode = 51 if production == 1
replace soccode = 53 if transportation == 1
replace soccode = 0 if soccode == .



drop if soccode == 0


gen occupation = .
replace occupation = 1 if soccode == 25 /*flexible, high-contact*/
replace occupation = 2 if (soccode >= 11 & soccode <=23)| soccode == 27 | soccode == 41 | soccode == 43 /*flexible, low-contact*/
replace occupation = 3 if soccode == 29 | soccode == 31 | soccode == 35 | soccode == 39  /*inflexible, high-contact*/
replace occupation = 4 if soccode == 33 | soccode == 37 | (soccode >= 45 & soccode <=53)
replace occupation = 5 if soccode == 0
label define occlab 1 "Flexible, High-Contact" 2 "Flexible, Low-Contact" 3 "Inflexible, High-Contact" 4 "Inflexible, Low-Contact" 5 "Occupation Unidentified" 



replace task_abstract = 0.0000001 if task_abstract == 0
replace task_routine = 0.0000001 if task_routine == 0
replace task_manual = 0.0000001 if task_manual == 0


keep occ2010 task_abstract task_routine task_manual _merge soccode occupation
gen lroutine = log(task_routine)
gen labstract = log(task_abstract)
gen lmanual = log(task_manual)

gen rti = lroutine - labstract - lmanual

egen abmedian = median(task_abstract)
egen rmedian = median(task_routine)
egen mmedian = median(task_manual)
egen tmedian = median(rti)

set sortseed 1004
bysort occ2010: egen num  = count(rti)
bysort occ2010: egen averti = mean(rti)
replace rti = averti if num > 1

gen abstract = task_abstract > abmedian
gen routine = task_routine > rmedian
gen manual = task_manual > mmedian
gen rtiscore = rti > tmedian 

keep occ2010 rtiscore occupation
duplicates drop
save "${proc}/rti", replace



