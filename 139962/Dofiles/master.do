include ../../config.do

clear 
set more off


********************************************************************************
* User sets globals
********************************************************************************

* Set up directory "main":
global main "U:\Workspace\aearep-2211\139962"

* Other directories:
global raw "${main}/Data/raw"
global proc "${main}/Data/processed"
global do "${main}/Dofiles"
global result "${main}/Results" 
global temp "${main}/Data/temp"
global fig "${main}/Figures"


********************************************************************************
* Occupation Classification (Table 1, Table 6)
********************************************************************************
do "${do}/rtido.do"
do "${do}/ONET_analysis.do"

********************************************************************************
* Data preparation
********************************************************************************
do "${do}/generate_data_for_stock.do" /*Table 2*/
do "${do}/generate_data_for_flow.do"
do "${do}/generate_data_for_stock_gc.do" /*Table 7*/
do "${do}/generate_data_for_flow_gc.do"
do "${do}/generate_EPseries.do" /*Figure 1-3 */



********************************************************************************
* Run regressions
********************************************************************************

do "${do}/regressions_covid19.do" /*Table 3-4, Figure 4-6*/
do "${do}/regressions_great_recession.do" /*Table 8, Figure 8*/


********************************************************************************
* Convert text files into excel
********************************************************************************

import delimited "${result}/share_demogroup_covid.txt", clear
rename v2 y2020
drop if y2020 == 100
export excel using "${result}/pop_distr.xlsx", cell(a3)
import delimited "${result}/share_demogroup_gr.txt", clear
drop demogroup
rename v2 y2007
drop if y2007 == 100
export excel using "${result}/pop_distr.xlsx", sheet("Sheet1",modify) cell(c3)



********************************************************************************
* Erase redundant files
********************************************************************************


erase "${result}/Corona_2digitocc.txt"
erase "${result}/Corona_Noocc.txt"
erase "${result}/Standard_2digitocc.txt"
erase "${result}/Standard_Noocc.txt"
erase "${result}/share_demogroup_covid.txt"
erase "${result}/share_demogroup_gr.txt"

erase "${temp}/EPseries_gender_15plus.dta"
erase "${temp}/occ_flexibility.dta"
erase "${temp}/workactivity.dta"
erase "${temp}/workcontext.dta"




log close

