
use "${raw}/cps_00074", clear


********************************************************************************
* demographics
********************************************************************************
keep if year >= 2007 & year <= 2012
bysort cpsidp: egen num = count(month)


keep if age >= 25 & age <= 55

gen married = .
replace married = 1 if marst == 1 /*Married, spouse present*/
replace married = 0 if marst >= 2 & marst <= 6


gen female = sex == 2

********************************************************************************
* employment status
********************************************************************************


gen empgroup = .
replace empgroup = 1 if empstat == 10 
replace empgroup = 1 if empstat == 12 & whyabsnt >= 5 & whyabsnt < 15
replace empgroup = 2 if empstat == 12 & whyabsnt == 15
replace empgroup = 3 if empstat >= 21 & empstat <= 22 
replace empgroup = 4 if empstat >= 32 & empstat <= 36 
replace empgroup = 5 if empgroup == .
label define emplab 1 "Employed" 2 "Temporarily Unemployed" 3 "Permanently Unemployed" 4 "NILF"
label values empgroup emplab


********************************************************************************
* education
********************************************************************************



gen edu2 = .
replace edu2 = 1 if educ < 73
replace edu2 = 2 if educ >= 73 & educ < 111
replace edu2 = 3 if educ >= 111 & educ <.



********************************************************************************
* occupation
********************************************************************************


gen management = 1 if occ2010 >= 10 & occ2010 <= 430
replace management = 0 if occ2010 < 10 | occ2010 > 430

gen business = 1 if occ2010 >= 500 & occ2010 <= 950
replace business = 0 if occ2010 < 500 | occ2010 > 950

gen compmath = 1 if occ2010 >= 1000 & occ2010 <= 1240
replace compmath = 0 if occ2010 < 1000 | occ2010 > 1240

gen engineer = 1 if occ2010 >= 1300 & occ2010 <= 1560
replace engineer = 0 if occ2010 < 1300 | occ2010 > 1560

gen research = 1 if occ2010 >= 1600 & occ2010 <= 1980
replace research = 0 if occ2010 < 1600 | occ2010 > 1980

gen socialservice = 1 if occ2010 >= 2000 & occ2010 <= 2060
replace socialservice = 0 if occ2010 < 2000 | occ2010 > 2060

gen legal = 1 if occ2010 >= 2100 & occ2010 <= 2160
replace legal = 0 if occ2010 < 2100 | occ2010 > 2160

gen education = 1 if occ2010 >= 2200 & occ2010 <= 2550
replace education = 0 if occ2010 < 2200 | occ2010 > 2550

gen arts = 1 if occ2010 >= 2600 & occ2010 <= 2920
replace arts = 0 if occ2010 < 2600 | occ2010 > 2920

gen healthtech = 1 if occ2010 >= 3000 & occ2010 <= 3540
replace healthtech = 0 if occ2010 < 3000 | occ2010 > 3540

gen healthsupport = 1 if occ2010 >= 3600 & occ2010 <= 3655
replace healthsupport = 0 if occ2010 < 3600 | occ2010 > 3655

gen protective = 1 if occ2010 >= 3700 & occ2010 <= 3955
replace protective = 0 if occ2010 < 3700 | occ2010 > 3955

gen foodprep = 1 if occ2010 >= 4000 & occ2010 <= 4150
replace foodprep = 0 if occ2010 < 4000 | occ2010 > 4150

gen cleaning = 1 if occ2010 >= 4200 & occ2010 <= 4250
replace cleaning = 0 if occ2010 < 4200 | occ2010 > 4250

gen personalcare = 1 if occ2010 >= 4300 & occ2010 <= 4650
replace personalcare = 0 if occ2010 < 4300 | occ2010 > 4650

gen sales = 1 if occ2010 >= 4700 & occ2010 <= 4965
replace sales = 0 if occ2010 < 4700 | occ2010 > 4965

gen office = 1 if occ2010 >= 5000 & occ2010 <= 5940
replace office = 0 if occ2010 < 5000 | occ2010 > 5940

gen farm = 1 if occ2010 >= 6005 & occ2010 <= 6130
replace farm = 0 if occ2010 < 6005 | occ2010 > 6130

gen construction = 1 if occ2010 >= 6200 & occ2010 <= 6940
replace construction = 0 if occ2010 < 6200 | occ2010 > 6940

gen installation = 1 if occ2010 >= 7000 & occ2010 <= 7630
replace installation = 0 if occ2010 < 7000 | occ2010 > 7630

gen production = 1 if occ2010 >= 7700 & occ2010 <= 8965
replace production = 0 if occ2010 < 7700 | occ2010 > 8965

gen transportation = 1 if occ2010 >= 9000 & occ2010 <= 9750
replace transportation = 0 if occ2010 < 9000 | occ2010 > 9750

gen military = 1 if occ2010 == 9830	
replace military = 0 if occ2010 != 9830



gen soccode = .
replace soccode = 11 if management == 1
replace soccode = 13 if business == 1
replace soccode = 15 if compmath == 1
replace soccode = 17 if engineer == 1
replace soccode = 19 if research == 1
replace soccode = 21 if socialservice == 1
replace soccode = 23 if legal == 1
replace soccode = 25 if education == 1
replace soccode = 27 if arts == 1
replace soccode = 29 if healthtech == 1
replace soccode = 31 if healthsupport == 1
replace soccode = 33 if protective == 1
replace soccode = 35 if foodprep == 1
replace soccode = 37 if cleaning == 1
replace soccode = 39 if personalcare == 1
replace soccode = 41 if sales == 1
replace soccode = 43 if office == 1
replace soccode = 45 if farm == 1
replace soccode = 47 if construction == 1
replace soccode = 49 if installation == 1
replace soccode = 51 if production == 1
replace soccode = 53 if transportation == 1
replace soccode = 0 if soccode == .



drop if empstat == 1 /*Armed Force*/
drop if military == 1 



gen occupation = .
replace occupation = 1 if soccode == 25 /*flexible, high-contact*/
replace occupation = 2 if (soccode >= 11 & soccode <=23)| soccode == 27 | soccode == 41 | soccode == 43 /*flexible, low-contact*/
replace occupation = 3 if soccode == 29 | soccode == 31 | soccode == 35 | soccode == 39  /*inflexible, high-contact*/
replace occupation = 4 if soccode == 33 | soccode == 37 | (soccode >= 45 & soccode <=53)
replace occupation = 5 if soccode == 0
label define occlab 1 "Flexible, High-Contact" 2 "Flexible, Low-Contact" 3 "Inflexible, High-Contact" 4 "Inflexible, Low-Contact" 5 "Occupation Unidentified" 
label values occupation occlab

********************************************************************************
* (Table 7) Gender distribution over occupations
********************************************************************************


preserve

keep if year == 2007 & month == 11
gen empf = empgroup == 1 & female == 1
gen empm = empgroup == 1 & female == 0
gen employed = empgroup == 1
gen male = 1 - female

set sortseed 1004
bysort month: egen sum_empf = sum(empf*wtfinl)
bysort month occupation: egen sum_empf_occ = sum(empf*wtfinl)


bysort month: egen sum_empm = sum(empm*wtfinl)
bysort month occupation: egen sum_empm_occ = sum(empm*wtfinl)


bysort month: egen sum_employed = sum(employed*wtfinl)
bysort month occupation: egen sum_employed_occ = sum(employed*wtfinl)


keep sum_* month occupation
duplicates drop




gen empshrf = sum_empf_occ/sum_empf*100
gen empshrm = sum_empm_occ/sum_empm*100
gen empshr = sum_employed_occ/sum_employed*100
gen fshr = sum_empf_occ/sum_employed_occ*100

keep occupation empshrf empshrm empshr fshr
drop if occupation == 5
*save "${result}/occ_distribution_gc"
export excel using "${result}/occ_distribution_gc.xlsx", firstrow(variables)


restore


********************************************************************************
* presence of children
********************************************************************************


gen children = nchild > 0 & yngch <= 12


gen demogroup = .
replace demogroup = 4 if female == 0 & married == 1 & children == 1 /*Married dads*/
replace demogroup = 3 if female == 0 & married == 1 & children == 0 /*Married men, no kids*/ 
replace demogroup = 2 if female == 0 & married == 0 & children == 1 /*Single dads*/
replace demogroup = 1 if female == 0 & married == 0 & children == 0 /*Single men, no kids*/
replace demogroup = 8 if female == 1 & married == 1 & children == 1 /*Married moms*/
replace demogroup = 7 if female == 1 & married == 1 & children == 0 /*Married women, no kids*/
replace demogroup = 6 if female == 1 & married == 0 & children == 1 /*Single moms*/
replace demogroup = 5 if female == 1 & married == 0 & children == 0 /*Single women, no kids*/

label define demo 4 "Married men w/ children" 3 "Married men w/o children" 2 "Single men w/ children" 1 "Single men w/o children" 8 "Married women w/ children" 7 "Married women w/o children" 6 "Single women w/ children" 5 "Single women w/o children"
label values demogroup demo

tabout demogroup using "${result}/share_demogroup_gr.txt" if year == 2007 & month >= 3 & month <= 11, c(col) append /*Table 9*/




********************************************************************************
* recession window
********************************************************************************



gen window = .
replace window = 1 if year == 2007 & month >= 3 & month <= 11
replace window = 2 if (year == 2007 & month == 12) | year == 2008 | (year == 2009 & month <= 6)
replace window = 3 if (year == 2009 & month > 6 & month <= 12) | year == 2010 
replace window = 4 if year == 2011 | (year == 2012 & month <= 7)

drop if window > 4

recode window (3/4=3), gen(newwindow)


drop month 
rename newwindow month





gen fmar = female*married
gen fchd = female*children
gen marchd = married*children
gen fmarchd = female*married*children


********************************************************************************
* age
********************************************************************************

gen agegroup = .
replace agegroup = 1 if age >= 25 & age <= 34
replace agegroup = 2 if age >= 35 & age <= 44
replace agegroup = 3 if age >= 45 & age <= 54


********************************************************************************
* stock variables
********************************************************************************


gen employment = empgroup == 1 
gen unemp = empgroup >= 2 & empgroup <= 3
gen nilf = empgroup == 4





xi i.month*i.soccode i.month*i.female i.month*i.married i.month*i.children i.month*i.fmar i.month*i.fchd i.month*i.marchd i.month*i.fmarchd i.month*i.agegroup i.month*i.edu2 i.year, noomit





*reg employment _Isoccode_13 _Isoccode_15 _Isoccode_17 _Isoccode_19 _Isoccode_21 _Isoccode_23 _Isoccode_25 _Isoccode_27 _Isoccode_29 _Isoccode_31 _Isoccode_33 _Isoccode_35 _Isoccode_37 _Isoccode_39 _Isoccode_41 _Isoccode_43 _Isoccode_45 _Isoccode_47 _Isoccode_49 _Isoccode_51 _Isoccode_53 _ImonXsoc_2_13 _ImonXsoc_2_15 _ImonXsoc_2_17 _ImonXsoc_2_19 _ImonXsoc_2_21 _ImonXsoc_2_23 _ImonXsoc_2_25 _ImonXsoc_2_27 _ImonXsoc_2_29 _ImonXsoc_2_31 _ImonXsoc_2_33 _ImonXsoc_2_35 _ImonXsoc_2_37 _ImonXsoc_2_39 _ImonXsoc_2_41 _ImonXsoc_2_43 _ImonXsoc_2_45 _ImonXsoc_2_47 _ImonXsoc_2_49 _ImonXsoc_2_51 _ImonXsoc_2_53 _ImonXsoc_3_13 _ImonXsoc_3_15 _ImonXsoc_3_17 _ImonXsoc_3_19 _ImonXsoc_3_21 _ImonXsoc_3_23 _ImonXsoc_3_25 _ImonXsoc_3_27 _ImonXsoc_3_29 _ImonXsoc_3_31 _ImonXsoc_3_33 _ImonXsoc_3_35 _ImonXsoc_3_37 _ImonXsoc_3_39 _ImonXsoc_3_41 _ImonXsoc_3_43 _ImonXsoc_3_45 _ImonXsoc_3_47 _ImonXsoc_3_49 _ImonXsoc_3_51 _ImonXsoc_3_53 _ImonXsoc_4_13 _ImonXsoc_4_15 _ImonXsoc_4_17 _ImonXsoc_4_19 _ImonXsoc_4_21 _ImonXsoc_4_23 _ImonXsoc_4_25 _ImonXsoc_4_27 _ImonXsoc_4_29 _ImonXsoc_4_31 _ImonXsoc_4_33 _ImonXsoc_4_35 _ImonXsoc_4_37 _ImonXsoc_4_39 _ImonXsoc_4_41 _ImonXsoc_4_43 _ImonXsoc_4_45 _ImonXsoc_4_47 _ImonXsoc_4_49 _ImonXsoc_4_51 _ImonXsoc_4_53 _Iagegroup_1 _Iagegroup_3 _ImonXage_2_1 _ImonXage_2_3 _ImonXage_3_1 _ImonXage_3_3 _ImonXage_4_1 _ImonXage_4_3 _Iedu2_1 _Iedu2_2 _ImonXedu_2_1 _ImonXedu_2_2 _ImonXedu_3_1 _ImonXedu_3_2 _ImonXedu_4_1 _ImonXedu_4_2 _Imonth_2 _Imonth_3 _Imonth_4 _Ifemale_1 _Imarried_1 _Ichildren_1 _Ifmar_1 _Ifchd_1 _Imarchd_1 _Ifmarchd_1 _ImonXfem_2_1 _ImonXfem_3_1 _ImonXfem_4_1 _ImonXmar_2_1 _ImonXmar_3_1 _ImonXmar_4_1 _ImonXchi_2_1 _ImonXchi_3_1 _ImonXchi_4_1 _ImonXfma_2_1 _ImonXfma_3_1 _ImonXfma_4_1 _ImonXfch_2_1 _ImonXfch_3_1 _ImonXfch_4_1 _ImonXmara2_1 _ImonXmara3_1 _ImonXmara4_1 _ImonXfmaa2_1 _ImonXfmaa3_1 _ImonXfmaa4_1 [pweight=wtfinl] if soccode>0, vce(robust)
*reg unemp _Isoccode_13 _Isoccode_15 _Isoccode_17 _Isoccode_19 _Isoccode_21 _Isoccode_23 _Isoccode_25 _Isoccode_27 _Isoccode_29 _Isoccode_31 _Isoccode_33 _Isoccode_35 _Isoccode_37 _Isoccode_39 _Isoccode_41 _Isoccode_43 _Isoccode_45 _Isoccode_47 _Isoccode_49 _Isoccode_51 _Isoccode_53 _ImonXsoc_2_13 _ImonXsoc_2_15 _ImonXsoc_2_17 _ImonXsoc_2_19 _ImonXsoc_2_21 _ImonXsoc_2_23 _ImonXsoc_2_25 _ImonXsoc_2_27 _ImonXsoc_2_29 _ImonXsoc_2_31 _ImonXsoc_2_33 _ImonXsoc_2_35 _ImonXsoc_2_37 _ImonXsoc_2_39 _ImonXsoc_2_41 _ImonXsoc_2_43 _ImonXsoc_2_45 _ImonXsoc_2_47 _ImonXsoc_2_49 _ImonXsoc_2_51 _ImonXsoc_2_53 _ImonXsoc_3_13 _ImonXsoc_3_15 _ImonXsoc_3_17 _ImonXsoc_3_19 _ImonXsoc_3_21 _ImonXsoc_3_23 _ImonXsoc_3_25 _ImonXsoc_3_27 _ImonXsoc_3_29 _ImonXsoc_3_31 _ImonXsoc_3_33 _ImonXsoc_3_35 _ImonXsoc_3_37 _ImonXsoc_3_39 _ImonXsoc_3_41 _ImonXsoc_3_43 _ImonXsoc_3_45 _ImonXsoc_3_47 _ImonXsoc_3_49 _ImonXsoc_3_51 _ImonXsoc_3_53 _ImonXsoc_4_13 _ImonXsoc_4_15 _ImonXsoc_4_17 _ImonXsoc_4_19 _ImonXsoc_4_21 _ImonXsoc_4_23 _ImonXsoc_4_25 _ImonXsoc_4_27 _ImonXsoc_4_29 _ImonXsoc_4_31 _ImonXsoc_4_33 _ImonXsoc_4_35 _ImonXsoc_4_37 _ImonXsoc_4_39 _ImonXsoc_4_41 _ImonXsoc_4_43 _ImonXsoc_4_45 _ImonXsoc_4_47 _ImonXsoc_4_49 _ImonXsoc_4_51 _ImonXsoc_4_53 _Iagegroup_1 _Iagegroup_3 _ImonXage_2_1 _ImonXage_2_3 _ImonXage_3_1 _ImonXage_3_3 _ImonXage_4_1 _ImonXage_4_3 _Iedu2_1 _Iedu2_2 _ImonXedu_2_1 _ImonXedu_2_2 _ImonXedu_3_1 _ImonXedu_3_2 _ImonXedu_4_1 _ImonXedu_4_2 _Imonth_2 _Imonth_3 _Imonth_4 _Ifemale_1 _Imarried_1 _Ichildren_1 _Ifmar_1 _Ifchd_1 _Imarchd_1 _Ifmarchd_1 _ImonXfem_2_1 _ImonXfem_3_1 _ImonXfem_4_1 _ImonXmar_2_1 _ImonXmar_3_1 _ImonXmar_4_1 _ImonXchi_2_1 _ImonXchi_3_1 _ImonXchi_4_1 _ImonXfma_2_1 _ImonXfma_3_1 _ImonXfma_4_1 _ImonXfch_2_1 _ImonXfch_3_1 _ImonXfch_4_1 _ImonXmara2_1 _ImonXmara3_1 _ImonXmara4_1 _ImonXfmaa2_1 _ImonXfmaa3_1 _ImonXfmaa4_1 [pweight=wtfinl] if soccode>0, vce(robust)
*reg nilf _Isoccode_13 _Isoccode_15 _Isoccode_17 _Isoccode_19 _Isoccode_21 _Isoccode_23 _Isoccode_25 _Isoccode_27 _Isoccode_29 _Isoccode_31 _Isoccode_33 _Isoccode_35 _Isoccode_37 _Isoccode_39 _Isoccode_41 _Isoccode_43 _Isoccode_45 _Isoccode_47 _Isoccode_49 _Isoccode_51 _Isoccode_53 _ImonXsoc_2_13 _ImonXsoc_2_15 _ImonXsoc_2_17 _ImonXsoc_2_19 _ImonXsoc_2_21 _ImonXsoc_2_23 _ImonXsoc_2_25 _ImonXsoc_2_27 _ImonXsoc_2_29 _ImonXsoc_2_31 _ImonXsoc_2_33 _ImonXsoc_2_35 _ImonXsoc_2_37 _ImonXsoc_2_39 _ImonXsoc_2_41 _ImonXsoc_2_43 _ImonXsoc_2_45 _ImonXsoc_2_47 _ImonXsoc_2_49 _ImonXsoc_2_51 _ImonXsoc_2_53 _ImonXsoc_3_13 _ImonXsoc_3_15 _ImonXsoc_3_17 _ImonXsoc_3_19 _ImonXsoc_3_21 _ImonXsoc_3_23 _ImonXsoc_3_25 _ImonXsoc_3_27 _ImonXsoc_3_29 _ImonXsoc_3_31 _ImonXsoc_3_33 _ImonXsoc_3_35 _ImonXsoc_3_37 _ImonXsoc_3_39 _ImonXsoc_3_41 _ImonXsoc_3_43 _ImonXsoc_3_45 _ImonXsoc_3_47 _ImonXsoc_3_49 _ImonXsoc_3_51 _ImonXsoc_3_53 _ImonXsoc_4_13 _ImonXsoc_4_15 _ImonXsoc_4_17 _ImonXsoc_4_19 _ImonXsoc_4_21 _ImonXsoc_4_23 _ImonXsoc_4_25 _ImonXsoc_4_27 _ImonXsoc_4_29 _ImonXsoc_4_31 _ImonXsoc_4_33 _ImonXsoc_4_35 _ImonXsoc_4_37 _ImonXsoc_4_39 _ImonXsoc_4_41 _ImonXsoc_4_43 _ImonXsoc_4_45 _ImonXsoc_4_47 _ImonXsoc_4_49 _ImonXsoc_4_51 _ImonXsoc_4_53 _Iagegroup_1 _Iagegroup_3 _ImonXage_2_1 _ImonXage_2_3 _ImonXage_3_1 _ImonXage_3_3 _ImonXage_4_1 _ImonXage_4_3 _Iedu2_1 _Iedu2_2 _ImonXedu_2_1 _ImonXedu_2_2 _ImonXedu_3_1 _ImonXedu_3_2 _ImonXedu_4_1 _ImonXedu_4_2 _Imonth_2 _Imonth_3 _Imonth_4 _Ifemale_1 _Imarried_1 _Ichildren_1 _Ifmar_1 _Ifchd_1 _Imarchd_1 _Ifmarchd_1 _ImonXfem_2_1 _ImonXfem_3_1 _ImonXfem_4_1 _ImonXmar_2_1 _ImonXmar_3_1 _ImonXmar_4_1 _ImonXchi_2_1 _ImonXchi_3_1 _ImonXchi_4_1 _ImonXfma_2_1 _ImonXfma_3_1 _ImonXfma_4_1 _ImonXfch_2_1 _ImonXfch_3_1 _ImonXfch_4_1 _ImonXmara2_1 _ImonXmara3_1 _ImonXmara4_1 _ImonXfmaa2_1 _ImonXfmaa3_1 _ImonXfmaa4_1 [pweight=wtfinl] if soccode>0, vce(robust)



save "${proc}/data_for_stock_gc"























