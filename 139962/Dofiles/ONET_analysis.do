


import delimited using "${raw}/Work Activities.txt", delim(tab) clear

keep if inlist(elementid,"4.A.3.a.1","4.A.3.a.2","4.A.3.a.3","4.A.3.a.4","4.A.4.a.8","4.A.3.b.4","4.A.3.b.5","4.A.1.b.2")

gen physical_activities = datavalue if elementid == "4.A.3.a.1" & scaleid=="IM"
label variable physical_activities "Performing General Physical Activities is very important"

gen handlingobjects = datavalue if elementid == "4.A.3.a.2" & scaleid=="IM"
label variable handlingobjects "Handling and Moving Objects is very important"

gen control_machines = datavalue if elementid == "4.A.3.a.3" & scaleid=="IM"
label variable control_machines "Controlling Machines and Processes [not computers nor vehicles] is very important"

gen operate_equipment = datavalue if elementid == "4.A.3.a.4" & scaleid=="IM"
label variable operate_equipment "Operating Vehicles, Mechanized Devices, or Equipment is very important"

gen dealwithpublic = datavalue if elementid == "4.A.4.a.8" & scaleid=="IM"
label variable dealwithpublic "Performing for or Working Directly with the Public is very important"

gen repair_mechequip = datavalue if elementid == "4.A.3.b.4" & scaleid=="IM"
label variable repair_mechequip "Repairing and Maintaining Mechanical Equipment is very important"

gen repair_elecequip = datavalue if elementid == "4.A.3.b.5" & scaleid=="IM"
label variable repair_elecequip "Repairing and Maintaining Electronic Equipment is very important"

gen inspect_equip = datavalue if elementid == "4.A.1.b.2" & scaleid=="IM"
label variable inspect_equip "Inspecting Equipment, Structures, or Materials is very important"

destring n, force replace

collapse (firstnm) physical_activities handlingobjects control_machines operate_equipment dealwithpublic repair_mechequip repair_elecequip inspect_equip (mean) n, by(onetsoccode)

save "${temp}/workactivity",replace








import delimited using "${raw}/Work Context.txt", delim(tab) clear

keep if inlist(elementid,"4.C.1.a.4","4.C.2.a.1.c","4.C.2.a.1.d","4.C.2.a.1.e","4.C.2.a.1.f","4.C.2.d.1.c","4.C.2.d.1.g","4.C.3.d.3")==1 | ///
inlist(elementid,"4.C.1.a.2.f","4.C.1.a.2.h","4.C.1.b.1.f","4.C.1.b.1.g","4.C.1.c.1","4.C.1.d.3","4.C.2.a.1.b","4.C.2.a.3")==1 | ///
inlist(elementid,"4.C.2.b.1.b","4.C.2.b.1.d","4.C.2.b.1.e","4.C.2.b.1.f","4.C.2.c.1.a","4.C.2.c.1.b","4.C.2.c.1.c","4.C.2.c.1.d")==1 | ///
inlist(elementid,"4.C.2.c.1.e","4.C.2.c.1.f","4.C.2.d.1.d","4.C.2.d.1.e","4.C.2.d.1.f","4.C.2.d.1.h","4.C.2.d.1.i","4.C.2.e.1.d")==1 | ///
inlist(elementid,"4.C.2.e.1.e","4.C.2.d.1.a","4.C.2.d.1.b")==1 | inlist(elementid, "4.C.3.b.2")==1

keep if scaleid=="CX" | inlist(elementid,"4.C.2.d.1.g","4.C.2.a.1.c","4.C.2.a.1.d")


replace datavalue = datavalue/100 if category != "n/a"
gen catdv = datavalue if category == "n/a"

destring category, force replace
replace catdv = category * datavalue if category >= 1 & category <= 5
bys onetsoccode elementid scaleid: egen score = sum(catdv)

drop catdv category datavalue standarderror lowercibound uppercibound
duplicates drop

keep if scaleid == "CX"


gen sitting_continually = score if elementid == "4.C.2.d.1.a" 
gen standing_continually = score if elementid == "4.C.2.d.1.b"
gen handsontools = score if elementid == "4.C.2.d.1.g"
gen outdoors_everyday = score if elementid == "4.C.2.a.1.c" | elementid == "4.C.2.a.1.d"

gen email_lessthanone = 6 - score if elementid == "4.C.1.a.2.h"
gen telephone_lessthanone = 6 - score if elementid == "4.C.1.a.2.f"

gen climbing_majority = score if elementid == "4.C.2.d.1.c"
gen walking_majority = score if elementid == "4.C.2.d.1.d"
gen crouching_majority = score if elementid=="4.C.2.d.1.e" 
gen keepingbalance_majority = score if elementid=="4.C.2.d.1.f" 
gen bendingbody_majority = score if elementid=="4.C.2.d.1.h" 
gen repetitivemotion_majority = score if elementid=="4.C.2.d.1.i" 
gen safetyequip_majority = score if elementid == "4.C.2.e.1.d" | elementid == "4.C.2.e.1.e"
gen contactothers_majority = score if elementid=="4.C.1.a.4" 
gen externalcustomer_veryimportant = score if elementid=="4.C.1.b.1.f" 
gen coordothers_veryimportant = score if elementid=="4.C.1.b.1.g" 
gen othershealth_veryimportant = score if elementid=="4.C.1.c.1" 
gen violentpeople_atleastweekly = score if elementid=="4.C.1.d.3" 
gen noac_everyday = score if elementid=="4.C.2.a.1.b" 
gen physicalclose_atleastmoderate = score if elementid=="4.C.2.a.3" 
gen extremetemp_everyday = score if elementid=="4.C.2.b.1.b" 
gen contaminant_atleastweekly = score if elementid=="4.C.2.b.1.d" 
gen crampedspace_everyday = score if elementid=="4.C.2.b.1.e" 
gen bodyvibration_atleastweekly = score if elementid=="4.C.2.b.1.f" 
gen radiation_atleastweekly = score if elementid=="4.C.2.c.1.a" 
gen disease_atleastweekly = score if elementid=="4.C.2.c.1.b" 
gen highplace_atleastweekly = score if elementid=="4.C.2.c.1.c" 
gen hazardcond_atleastweekly = score if elementid=="4.C.2.c.1.d" 
gen hazardequip_atleastweekly = score if elementid=="4.C.2.c.1.e" 
gen minorhurt_atleastweekly = score if elementid=="4.C.2.c.1.f" 

gen automated = score if elementid == "4.C.3.b.2"

keep onetsoccode automated sitting_continually standing_continually handsontools outdoors_everyday email_lessthanone telephone_lessthanone climbing_majority walking_majority crouching_majority keepingbalance_majority bendingbody_majority repetitivemotion_majority safetyequip_majority contactothers_majority externalcustomer_veryimportant coordothers_veryimportant othershealth_veryimportant violentpeople_atleastweekly noac_everyday physicalclose_atleastmoderate extremetemp_everyday contaminant_atleastweekly crampedspace_everyday bodyvibration_atleastweekly radiation_atleastweekly disease_atleastweekly highplace_atleastweekly hazardcond_atleastweekly hazardequip_atleastweekly minorhurt_atleastweekly
duplicates drop

collapse (firstnm) automated sitting_continually standing_continually handsontools outdoors_everyday email_lessthanone telephone_lessthanone climbing_majority walking_majority crouching_majority keepingbalance_majority bendingbody_majority repetitivemotion_majority safetyequip_majority contactothers_majority externalcustomer_veryimportant coordothers_veryimportant othershealth_veryimportant violentpeople_atleastweekly noac_everyday physicalclose_atleastmoderate extremetemp_everyday contaminant_atleastweekly crampedspace_everyday bodyvibration_atleastweekly radiation_atleastweekly disease_atleastweekly highplace_atleastweekly hazardcond_atleastweekly hazardequip_atleastweekly minorhurt_atleastweekly, by(onetsoccode)


save "${temp}/workcontext", replace

merge 1:1 onetsoccode using "${temp}/workactivity"

drop n _merge

merge 1:1 onetsoccode using "${raw}/occlist"
drop if _merge == 2
drop _merge
order title, after(onetsoccode)


save "${temp}/occ_flexibility", replace







egen flex = rowmean(email_lessthanone outdoors_everyday violentpeople_atleastweekly safetyequip_majority minorhurt_atleastweekly physical_activities handlingobjects control_machines operate_equipment dealwithpublic repair_mechequip repair_elecequip inspect_equip disease_atleastweekly walking_majority)
gen prox = physicalclose_atleastmoderate


gen indcode = substr(onetsoccode,1,2)
gen soccode = substr(onetsoccode,1,7)
destring indcode, force replace




merge m:1 soccode using "${raw}/crosswalk"
drop if _merge == 2
drop _merge
merge m:1 occ2010 using "${raw}/empshare"
drop if _merge == 2
drop _merge


bysort indcode: egen inflexibility = wtmean(flex), weight(empshare)
bysort indcode: egen proximity = wtmean(prox), weight(empshare)

keep indcode inflexibility proximity 
duplicates drop
*save "${result}/occ_by_flex_cont"
export excel using "${result}/occ_by_flex_cont.xlsx", firstrow(variables) replace

egen fmedian = pctile(inflexibility), p(50)
egen pmedian = pctile(proximity), p(50) 



gen group = .
replace group = 1 if inflexibility <= fmedian & proximity >= 4 /*flexible, high*/
replace group = 2 if inflexibility <= fmedian & proximity < 4/*flexible, low*/
replace group = 3 if inflexibility > fmedian & proximity >= 4/*inflexible, high*/
replace group = 4 if inflexibility > fmedian & proximity < 4/*inflexible, low*/

sort group indcode

label define occlab 1 "Flexible,High-contact" 2 "Flexible,Low-contact" 3 "Inflexible,High-contact" 4 "Inflexible,Low-contact"
label values group occlab


*save "${result}/classification"
export excel using "${result}/classification.xlsx", firstrow(variables) replace







