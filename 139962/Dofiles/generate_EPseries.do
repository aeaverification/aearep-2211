


forvalues i=1(1)3{

use "${raw}/cps_00075",clear

gen agegroup = .
replace agegroup = 1 if age >= 15 & age <= 24
replace agegroup = 2 if age >= 25 & age <= 55
replace agegroup = 3 if age > 55

keep if agegroup == `i' 

gen married = .
replace married = 1 if marst == 1 /*Married, spouse present*/
replace married = 0 if marst >= 2 & marst <= 7


gen demogroup = .
replace demogroup = 1 if sex == 1 & married == 1 
replace demogroup = 2 if sex == 1 & married == 0 
replace demogroup = 3 if sex == 2 & married == 1 
replace demogroup = 4 if sex == 2 & married == 0 
label define demolab 1 "Married Men" 2 "Single Men" 3 "Married Women" 4 "Single Women"
label values demogroup demolab


gen gender = .
replace gender = 1 if sex == 1 
replace gender = 2 if sex == 2
label define sexlab 1 "Male" 2 "Female"
label values gender sexlab

gen empgroup = .
replace empgroup = 1 if empstat >= 10 & empstat <= 12
replace empgroup = 2 if empstat == 12 & whyabsnt == 15
replace empgroup = 3 if empstat >= 21 & empstat <= 22 
replace empgroup = 4 if empstat >= 32 & empstat <= 36 
replace empgroup = 5 if empgroup == .
label define emplab 1 "Employed" 2 "Temporarily Unemployed" 3 "Permanently Unemployed" 4 "NILF"
label values empgroup emplab


gen children = nchild > 0 & yngch <= 12

gen management = 1 if occ2010 >= 10 & occ2010 <= 430
replace management = 0 if occ2010 < 10 | occ2010 > 430

gen business = 1 if occ2010 >= 500 & occ2010 <= 950
replace business = 0 if occ2010 < 500 | occ2010 > 950

gen compmath = 1 if occ2010 >= 1000 & occ2010 <= 1240
replace compmath = 0 if occ2010 < 1000 | occ2010 > 1240

gen engineer = 1 if occ2010 >= 1300 & occ2010 <= 1560
replace engineer = 0 if occ2010 < 1300 | occ2010 > 1560

gen research = 1 if occ2010 >= 1600 & occ2010 <= 1980
replace research = 0 if occ2010 < 1600 | occ2010 > 1980

gen socialservice = 1 if occ2010 >= 2000 & occ2010 <= 2060
replace socialservice = 0 if occ2010 < 2000 | occ2010 > 2060

gen legal = 1 if occ2010 >= 2100 & occ2010 <= 2160
replace legal = 0 if occ2010 < 2100 | occ2010 > 2160

gen education = 1 if occ2010 >= 2200 & occ2010 <= 2550
replace education = 0 if occ2010 < 2200 | occ2010 > 2550

gen arts = 1 if occ2010 >= 2600 & occ2010 <= 2920
replace arts = 0 if occ2010 < 2600 | occ2010 > 2920

gen healthtech = 1 if occ2010 >= 3000 & occ2010 <= 3540
replace healthtech = 0 if occ2010 < 3000 | occ2010 > 3540

gen healthsupport = 1 if occ2010 >= 3600 & occ2010 <= 3655
replace healthsupport = 0 if occ2010 < 3600 | occ2010 > 3655

gen protective = 1 if occ2010 >= 3700 & occ2010 <= 3955
replace protective = 0 if occ2010 < 3700 | occ2010 > 3955

gen foodprep = 1 if occ2010 >= 4000 & occ2010 <= 4150
replace foodprep = 0 if occ2010 < 4000 | occ2010 > 4150

gen cleaning = 1 if occ2010 >= 4200 & occ2010 <= 4250
replace cleaning = 0 if occ2010 < 4200 | occ2010 > 4250

gen personalcare = 1 if occ2010 >= 4300 & occ2010 <= 4650
replace personalcare = 0 if occ2010 < 4300 | occ2010 > 4650

gen sales = 1 if occ2010 >= 4700 & occ2010 <= 4965
replace sales = 0 if occ2010 < 4700 | occ2010 > 4965

gen office = 1 if occ2010 >= 5000 & occ2010 <= 5940
replace office = 0 if occ2010 < 5000 | occ2010 > 5940

gen farm = 1 if occ2010 >= 6005 & occ2010 <= 6130
replace farm = 0 if occ2010 < 6005 | occ2010 > 6130

gen construction = 1 if occ2010 >= 6200 & occ2010 <= 6940
replace construction = 0 if occ2010 < 6200 | occ2010 > 6940

gen installation = 1 if occ2010 >= 7000 & occ2010 <= 7630
replace installation = 0 if occ2010 < 7000 | occ2010 > 7630

gen production = 1 if occ2010 >= 7700 & occ2010 <= 8965
replace production = 0 if occ2010 < 7700 | occ2010 > 8965

gen transportation = 1 if occ2010 >= 9000 & occ2010 <= 9750
replace transportation = 0 if occ2010 < 9000 | occ2010 > 9750

gen military = 1 if occ2010 == 9830	
replace military = 0 if occ2010 != 9830



drop if empstat == 1 /*Armed Force*/
drop if military == 1 

gen employed = empgroup == 1



preserve

bysort year month sex: egen P = sum(wtfinl)
bysort year month sex employed: egen E = sum(wtfinl)
keep year month sex employed P E
duplicates drop

keep if employed == 1

gen EPratio = E/P*100

drop employed
sort sex year month
save "${temp}/EPseries_gender_`i'"


restore


drop if year < 1982


bysort year month demogroup children: egen P = sum(wtfinl)
bysort year month demogroup children employed: egen E = sum(wtfinl)
keep year month demogroup children employed P E
duplicates drop

keep if employed == 1

gen EPratio = E/P*100

drop employed
sort demogroup children year month 
save "${temp}/EPseries_children_`i'"
}

* Just formatting..
use "${temp}/EPseries_gender_1",clear
append using "${temp}/EPseries_gender_2"
append using "${temp}/EPseries_gender_3"
sort sex year month 
bysort sex year month: egen Psum = sum(P)
bysort sex year month: egen Esum = sum(E)
drop P E EPratio
duplicates drop
rename (Psum Esum) (P E)
gen EPratio = E/P*100
save "${temp}/EPseries_gender_15plus"



use "${temp}/EPseries_gender_1",clear
rename (P E EPratio) (P1524 E1524 EPratio1524)
export excel using "${result}/EPseries_monthly_sa_all.xlsx", firstrow(variable) cell(A2)
use "${temp}/EPseries_gender_3",clear
drop year month sex
rename (P E EPratio) (P55plus E55plus EPratio55plus)
export excel using "${result}/EPseries_monthly_sa_all.xlsx", sheet("Sheet1", modify) firstrow(variable) cell(g2)
use "${temp}/EPseries_gender_2",clear
drop year month sex
rename (P E EPratio) (P2555 E2555 EPratio2555)
export excel using "${result}/EPseries_monthly_sa_all.xlsx", sheet("Sheet1", modify) firstrow(variable) cell(j2)
use "${temp}/EPseries_gender_15plus",clear
drop year month sex
export excel using "${result}/EPseries_monthly_sa_all.xlsx", sheet("Sheet1", modify) firstrow(variable) cell(m2)



use "${temp}/EPseries_children_1",clear
append using "${temp}/EPseries_children_2"
append using "${temp}/EPseries_children_3"
bysort demogroup children year month: egen Psum = sum(P)
bysort demogroup children year month: egen Esum = sum(E)
drop P E EPratio
duplicates drop
rename (Psum Esum) (P E)
gen EPratio = E/P*100
sort demogroup children year month 
export excel using "${result}/EPseries_monthly_15andolder.xlsx", firstrow(variable)

erase "${temp}/EPseries_children_1.dta"
erase "${temp}/EPseries_children_2.dta" 
erase "${temp}/EPseries_children_3.dta" 

erase "${temp}/EPseries_gender_1.dta"
erase "${temp}/EPseries_gender_2.dta" 
erase "${temp}/EPseries_gender_3.dta" 





/*****************************************************************************************

use "${temp}/rawdb", clear

gen employed = empgroup == 1

bysort year month demogroup: egen P = sum(wtfinl)
bysort year month demogroup employed: egen E = sum(wtfinl)
keep year month demogroup employed P E
duplicates drop

keep if employed == 1

gen EPratio = E/P*100

drop employed
sort demogroup year month
export excel using "${proc}/EPseries_monthly.xlsx", sheet("GenderMarital",replace) firstrow(variables) 

*collapse (mean) EPratio, by(year demogroup)
*sort demogroup year 
*export excel using "${proc}/EPseries_yearly.xlsx", sheet("GenderMarital",replace) firstrow(variables) 

*****************************************************************************************/


use "${raw}/cps_00075",clear


drop if year < 2019
drop if year == 2020 & month == 12


gen agegroup = .
replace agegroup = 1 if age >= 15 & age <= 24
replace agegroup = 2 if age >= 25 & age <= 55
replace agegroup = 3 if age > 55

keep if agegroup == 2

gen married = .
replace married = 1 if marst == 1 /*Married, spouse present*/
replace married = 0 if marst >= 2 & marst <= 7


gen demogroup = .
replace demogroup = 1 if sex == 1 & married == 1 
replace demogroup = 2 if sex == 1 & married == 0 
replace demogroup = 3 if sex == 2 & married == 1 
replace demogroup = 4 if sex == 2 & married == 0 
label define demolab 1 "Married Men" 2 "Single Men" 3 "Married Women" 4 "Single Women"
label values demogroup demolab


gen gender = .
replace gender = 1 if sex == 1 
replace gender = 2 if sex == 2
label define sexlab 1 "Male" 2 "Female"
label values gender sexlab

gen empgroup = .
replace empgroup = 1 if empstat >= 10 & empstat <= 12
replace empgroup = 2 if empstat == 12 & whyabsnt == 15
replace empgroup = 3 if empstat >= 21 & empstat <= 22 
replace empgroup = 4 if empstat >= 32 & empstat <= 36 
replace empgroup = 5 if empgroup == .
label define emplab 1 "Employed" 2 "Temporarily Unemployed" 3 "Permanently Unemployed" 4 "NILF"
label values empgroup emplab


gen children = nchild > 0 & yngch <= 12

gen management = 1 if occ2010 >= 10 & occ2010 <= 430
replace management = 0 if occ2010 < 10 | occ2010 > 430

gen business = 1 if occ2010 >= 500 & occ2010 <= 950
replace business = 0 if occ2010 < 500 | occ2010 > 950

gen compmath = 1 if occ2010 >= 1000 & occ2010 <= 1240
replace compmath = 0 if occ2010 < 1000 | occ2010 > 1240

gen engineer = 1 if occ2010 >= 1300 & occ2010 <= 1560
replace engineer = 0 if occ2010 < 1300 | occ2010 > 1560

gen research = 1 if occ2010 >= 1600 & occ2010 <= 1980
replace research = 0 if occ2010 < 1600 | occ2010 > 1980

gen socialservice = 1 if occ2010 >= 2000 & occ2010 <= 2060
replace socialservice = 0 if occ2010 < 2000 | occ2010 > 2060

gen legal = 1 if occ2010 >= 2100 & occ2010 <= 2160
replace legal = 0 if occ2010 < 2100 | occ2010 > 2160

gen education = 1 if occ2010 >= 2200 & occ2010 <= 2550
replace education = 0 if occ2010 < 2200 | occ2010 > 2550

gen arts = 1 if occ2010 >= 2600 & occ2010 <= 2920
replace arts = 0 if occ2010 < 2600 | occ2010 > 2920

gen healthtech = 1 if occ2010 >= 3000 & occ2010 <= 3540
replace healthtech = 0 if occ2010 < 3000 | occ2010 > 3540

gen healthsupport = 1 if occ2010 >= 3600 & occ2010 <= 3655
replace healthsupport = 0 if occ2010 < 3600 | occ2010 > 3655

gen protective = 1 if occ2010 >= 3700 & occ2010 <= 3955
replace protective = 0 if occ2010 < 3700 | occ2010 > 3955

gen foodprep = 1 if occ2010 >= 4000 & occ2010 <= 4150
replace foodprep = 0 if occ2010 < 4000 | occ2010 > 4150

gen cleaning = 1 if occ2010 >= 4200 & occ2010 <= 4250
replace cleaning = 0 if occ2010 < 4200 | occ2010 > 4250

gen personalcare = 1 if occ2010 >= 4300 & occ2010 <= 4650
replace personalcare = 0 if occ2010 < 4300 | occ2010 > 4650

gen sales = 1 if occ2010 >= 4700 & occ2010 <= 4965
replace sales = 0 if occ2010 < 4700 | occ2010 > 4965

gen office = 1 if occ2010 >= 5000 & occ2010 <= 5940
replace office = 0 if occ2010 < 5000 | occ2010 > 5940

gen farm = 1 if occ2010 >= 6005 & occ2010 <= 6130
replace farm = 0 if occ2010 < 6005 | occ2010 > 6130

gen construction = 1 if occ2010 >= 6200 & occ2010 <= 6940
replace construction = 0 if occ2010 < 6200 | occ2010 > 6940

gen installation = 1 if occ2010 >= 7000 & occ2010 <= 7630
replace installation = 0 if occ2010 < 7000 | occ2010 > 7630

gen production = 1 if occ2010 >= 7700 & occ2010 <= 8965
replace production = 0 if occ2010 < 7700 | occ2010 > 8965

gen transportation = 1 if occ2010 >= 9000 & occ2010 <= 9750
replace transportation = 0 if occ2010 < 9000 | occ2010 > 9750

gen military = 1 if occ2010 == 9830	
replace military = 0 if occ2010 != 9830



drop if empstat == 1 /*Armed Force*/
drop if military == 1 

gen employed = empgroup == 1




gen soccode = .
replace soccode = 11 if management == 1
replace soccode = 13 if business == 1
replace soccode = 15 if compmath == 1
replace soccode = 17 if engineer == 1
replace soccode = 19 if research == 1
replace soccode = 21 if socialservice == 1
replace soccode = 23 if legal == 1
replace soccode = 25 if education == 1
replace soccode = 27 if arts == 1
replace soccode = 29 if healthtech == 1
replace soccode = 31 if healthsupport == 1
replace soccode = 33 if protective == 1
replace soccode = 35 if foodprep == 1
replace soccode = 37 if cleaning == 1
replace soccode = 39 if personalcare == 1
replace soccode = 41 if sales == 1
replace soccode = 43 if office == 1
replace soccode = 45 if farm == 1
replace soccode = 47 if construction == 1
replace soccode = 49 if installation == 1
replace soccode = 51 if production == 1
replace soccode = 53 if transportation == 1
replace soccode = 0 if soccode == .




gen occupation = .
replace occupation = 1 if soccode == 25 /*flexible, high-contact*/
replace occupation = 2 if (soccode >= 11 & soccode <=23)| soccode == 27 | soccode == 41 | soccode == 43 /*flexible, low-contact*/
replace occupation = 3 if soccode == 29 | soccode == 31 | soccode == 35 | soccode == 39  /*inflexible, high-contact*/
replace occupation = 4 if soccode == 33 | soccode == 37 | (soccode >= 45 & soccode <=53)
replace occupation = 5 if soccode == 0
label define occlab 1 "Flexible, High-Contact" 2 "Flexible, Low-Contact" 3 "Inflexible, High-Contact" 4 "Inflexible, Low-Contact" 5 "Occupation Unidentified" 
label values occupation occlab



set sortseed 1004
bysort year month gender: egen P = sum(wtfinl)
bysort year month gender occupation empgroup: egen E = sum(wtfinl)
keep year month gender occupation empgroup P E
duplicates drop
reshape wide P E, i(year month gender occupation) j(empgroup)
drop P2 P3 P4
rename P1 P
rename (E1 E2 E3 E4) (pop_employed pop_furloughed pop_unemployed pop_nilf)
drop if occupation == 5
order P, after(pop_nilf)
rename P pop_total


foreach v in pop_employed pop_furloughed pop_unemployed pop_nilf{
replace `v' = 0 if `v' == .
}

gen EPratio = pop_employed/pop_total*100
gen UPratio = pop_unemployed/pop_total*100
gen NPratio = pop_nilf/pop_total*100
gen TPratio = pop_furloughed/pop_total*100
sort gender occupation year month
export excel using "${result}/CPS_COVID_EMPLOYMENT_NEW.xlsx", sheet("GenderOccupation",replace) firstrow(variables)

drop EPratio UPratio NPratio TPratio

reshape wide pop_employed pop_furloughed pop_unemployed pop_nilf pop_total, i(year month occupation) j(gender)

foreach var in pop_employed pop_furloughed pop_unemployed pop_nilf pop_total{
gen `var' = `var'1 + `var'2
}

drop pop_employed1 pop_furloughed1 pop_unemployed1 pop_nilf1 pop_total1 pop_employed2 pop_furloughed2 pop_unemployed2 pop_nilf2 pop_total2 
gen EPratio = pop_employed/pop_total*100
gen TPratio = pop_furloughed/pop_total*100
gen UPratio = pop_unemployed/pop_total*100
gen NPratio = pop_nilf/pop_total*100
sort occupation year month
export excel using "${result}/CPS_COVID_EMPLOYMENT_NEW.xlsx", sheet("Occupation",replace) firstrow(variables)



























