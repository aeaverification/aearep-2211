




********************************************************************************
* Flow regressions
********************************************************************************


foreach dvar in en un eu nu ue ne nn uu ee{
use "${proc}/data_for_flow.dta", clear

# delimit;
reg `dvar' _Isoccode_13 _Isoccode_15 _Isoccode_17 _Isoccode_19 _Isoccode_21 _Isoccode_23 _Isoccode_25 
_Isoccode_27 _Isoccode_29 _Isoccode_31 _Isoccode_33 _Isoccode_35 _Isoccode_37 _Isoccode_39 _Isoccode_41 
_Isoccode_43 _Isoccode_45 _Isoccode_47 _Isoccode_49 _Isoccode_51 _Isoccode_53 
_ImonXsoc_2_13 _ImonXsoc_2_15 _ImonXsoc_2_17 _ImonXsoc_2_19 _ImonXsoc_2_21 _ImonXsoc_2_23 _ImonXsoc_2_25 _ImonXsoc_2_27 _ImonXsoc_2_29 
_ImonXsoc_2_31 _ImonXsoc_2_33 _ImonXsoc_2_35 _ImonXsoc_2_37 _ImonXsoc_2_39 _ImonXsoc_2_41 _ImonXsoc_2_43 _ImonXsoc_2_45 _ImonXsoc_2_47 
_ImonXsoc_2_49 _ImonXsoc_2_51 _ImonXsoc_2_53 
_ImonXsoc_3_13 _ImonXsoc_3_15 _ImonXsoc_3_17 _ImonXsoc_3_19 _ImonXsoc_3_21 _ImonXsoc_3_23 _ImonXsoc_3_25 _ImonXsoc_3_27 _ImonXsoc_3_29 
_ImonXsoc_3_31 _ImonXsoc_3_33 _ImonXsoc_3_35 _ImonXsoc_3_37 _ImonXsoc_3_39 _ImonXsoc_3_41 _ImonXsoc_3_43 _ImonXsoc_3_45 _ImonXsoc_3_47 
_ImonXsoc_3_49 _ImonXsoc_3_51 _ImonXsoc_3_53 _Iagegroup_1 _Iagegroup_3 _ImonXage_2_1 _ImonXage_2_3 _ImonXage_3_1 _ImonXage_3_3 
_Iedu2_1 _Iedu2_2 _ImonXedu_2_1 _ImonXedu_2_2 _ImonXedu_3_1 _ImonXedu_3_2 _Imonth_2 _Imonth_3 
_Ifemale_1 _Imarried_1 _Ichildren_1 _Ifmar_1 _Ifchd_1 _Imarchd_1 _Ifmarchd_1 _ImonXfem_2_1 _ImonXfem_3_1 
_ImonXmar_2_1 _ImonXmar_3_1 _ImonXchi_2_1 _ImonXchi_3_1 _ImonXfma_2_1 _ImonXfma_3_1 
_ImonXfch_2_1 _ImonXfch_3_1 _ImonXmara2_1 _ImonXmara3_1 _ImonXfmaa2_1 _ImonXfmaa3_1 [pweight=wtfinl] if soccode>0, vce(robust);
# delimit cr






*men, single w/o children
lincom _b[_Imonth_2] 
local se11=r(se)
local coef11=r(estimate)

lincom _b[_Imonth_3] 
local se12=r(se)
local coef12=r(estimate)



*men, single w/ children
lincom _b[_Imonth_2] + _b[_ImonXchi_2_1]
local se21=r(se)
local coef21=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXchi_3_1]
local se22=r(se)
local coef22=r(estimate)




*men, married w/o children
lincom _b[_Imonth_2] + _b[_ImonXmar_2_1]
local se31=r(se)
local coef31=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXmar_3_1]
local se32=r(se)
local coef32=r(estimate)




*men, married w/ children
lincom _b[_Imonth_2] + _b[_ImonXmar_2_1] +_b[_ImonXchi_2_1] +_b[ _ImonXmara2_1]
local se41=r(se)
local coef41=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXmar_3_1] +_b[_ImonXchi_3_1] +_b[ _ImonXmara3_1]
local se42=r(se)
local coef42=r(estimate)




*women, single w/o children
lincom _b[_Imonth_2] + _b[_ImonXfem_2_1]
local se51=r(se)
local coef51=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXfem_3_1]
local se52=r(se)
local coef52=r(estimate)




*women, single w/ children
lincom _b[_Imonth_2] + _b[_ImonXchi_2_1]+ _b[_ImonXfem_2_1] + _b[_ImonXfch_2_1]
local se61=r(se)
local coef61=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXchi_3_1]+ _b[_ImonXfem_3_1] + _b[_ImonXfch_3_1]
local se62=r(se)
local coef62=r(estimate)




*women, married w/o children
lincom _b[_Imonth_2] + _b[_ImonXmar_2_1]+ _b[_ImonXfem_2_1] + _b[_ImonXfma_2_1]
local se71=r(se)
local coef71=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXmar_3_1]+ _b[_ImonXfem_3_1] + _b[_ImonXfma_3_1]
local se72=r(se)
local coef72=r(estimate)




*women, married w/ children
lincom _b[_Imonth_2] + _b[_ImonXmar_2_1] +_b[_ImonXchi_2_1] +_b[ _ImonXmara2_1]+ _b[_ImonXfem_2_1] + _b[_ImonXfmaa2_1]+ _b[_ImonXfma_2_1]+ _b[_ImonXfch_2_1]
local se81=r(se)
local coef81=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXmar_3_1] +_b[_ImonXchi_3_1] +_b[ _ImonXmara3_1]+ _b[_ImonXfem_3_1] + _b[_ImonXfmaa3_1]+ _b[_ImonXfma_3_1]+ _b[_ImonXfch_3_1]
local se82=r(se)
local coef82=r(estimate)



*diff, single w/o children
lincom (_b[_Imonth_2] + _b[_ImonXfem_2_1]) - _b[_Imonth_2]
local se91=r(se)
local coef91=r(estimate)

lincom (_b[_Imonth_3] + _b[_ImonXfem_3_1]) - _b[_Imonth_3]
local se92=r(se)
local coef92=r(estimate)




*diff, single w/ children
lincom _b[_ImonXfem_2_1] + _b[_ImonXfch_2_1]
local se101=r(se)
local coef101=r(estimate)

lincom _b[_ImonXfem_3_1] + _b[_ImonXfch_3_1]
local se102=r(se)
local coef102=r(estimate)



*diff, married w/o children
lincom _b[_ImonXfem_2_1] + _b[_ImonXfma_2_1]
local se111=r(se)
local coef111=r(estimate)

lincom _b[_ImonXfem_3_1] + _b[_ImonXfma_3_1]
local se112=r(se)
local coef112=r(estimate)




*diff, married w/ children
lincom _b[_ImonXfem_2_1] + _b[_ImonXfmaa2_1]+ _b[_ImonXfma_2_1]+ _b[_ImonXfch_2_1]
local se121=r(se)
local coef121=r(estimate)

lincom _b[_ImonXfem_3_1] + _b[_ImonXfmaa3_1]+ _b[_ImonXfma_3_1]+ _b[_ImonXfch_3_1]
local se122=r(se)
local coef122=r(estimate)

********************************************************************************
*recession period: March 2020 - May 2020
*recovery period: June 2020 - Nov 2020
********************************************************************************


# delimit ;
outreg2 using "${result}\Corona_2digitocc", append excel noaster addstat("coef_singlemenwithoutchildren_recession",`coef11',"coef_singlemenwithoutchildren_recovery",`coef12',
"coef_singlemenwithchildren_recession",`coef21',"coef_singlemenwithchildren_recovery",`coef22',
"coef_marriedmenwithoutchildren_recession",`coef31',"coef_marriedmenwithoutchildren_recovery",`coef32',
"coef_marriedmenwithchildren_recession",`coef41',"coef_marriedmenwithchildren_recovery",`coef42',
"coef_singlewomenwithoutchildren_recession",`coef51',"coef_singlewomenwithoutchildren_recovery",`coef52',
"coef_singlewomenwithchildren_recession",`coef61',"coef_singlewomenwithchildren_recovery",`coef62',
"coef_marriedwomenwithoutchildren_recession",`coef71',"coef_marriedwomenwithoutchildren_recovery",`coef72',
"coef_marriedwomenwithchildren_recession",`coef81',"coef_marriedwomenwithchildren_recovery",`coef82',
"diff_singlewithoutchildren_recession",`coef91',"diff_singlewithoutchildren_recovery",`coef92',
"diff_singlewomenwithchildren_recession",`coef101',"diff_singlewithchildren_recovery",`coef102',
"diff_marriedwomenwithoutchildren_recession",`coef111',"diff_marriedwithoutchildren_recovery",`coef112',
"diff_marriedwomenwithchildren_recession",`coef121',"diff_marriedwithchildren_recovery",`coef122',
"se_singlemenwithoutchildren_recession",`se11',"se_singlemenwithoutchildren_recovery",`se12',
"se_singlemenwithchildren_recession",`se21',"se_singlemenwithchildren_recovery",`se22',
"se_marriedmenwithoutchildren_recession",`se31',"se_marriedmenwithoutchildren_recovery",`se32',
"se_marriedmenwithchildren_recession",`se41',"se_marriedmenwithchildren_recovery",`se42',
"se_singlewomenwithoutchildren_recession",`se51',"se_singlewomenwithoutchildren_recovery",`se52',
"se_singlewomenwithchildren_recession",`se61',"se_singlewomenwithchildren_recovery",`se62',
"se_marriedwomenwithoutchildren_recession",`se71',"se_marriedwomenwithoutchildren_recovery",`se72',
"se_marriedwomenwithchildren_recession",`se81',"se_marriedwomenwithchildren_recovery",`se82',
"diffse_singlewithoutchildren_recession",`se91',"diffse_singlewithoutchildren_recovery",`se92',
"diffse_singlewomenwithchildren_recession",`se101',"diffse_singlewithchildren_recovery",`se102',
"diffse_marriedwomenwithoutchildren_recession",`se111',"diffse_marriedwithoutchildren_recovery",`se112',
"diffse_marriedwomenwithchildren_recession",`se121',"diffse_marriedwithchildren_recovery",`se122');
# delimit cr

local drop se*


* No occupation controls
# delimit ;
reg `dvar' _Iagegroup_1 _Iagegroup_3 _ImonXage_2_1 _ImonXage_2_3 _ImonXage_3_1 _ImonXage_3_3 
_Iedu2_1 _Iedu2_2 _ImonXedu_2_1 _ImonXedu_2_2 _ImonXedu_3_1 _ImonXedu_3_2 
_Imonth_2 _Imonth_3 
_Ifemale_1 _Imarried_1 _Ichildren_1 _Ifmar_1 _Ifchd_1 _Imarchd_1 _Ifmarchd_1 
_ImonXfem_2_1 _ImonXfem_3_1 
_ImonXmar_2_1 _ImonXmar_3_1 
_ImonXchi_2_1 _ImonXchi_3_1 
_ImonXfma_2_1 _ImonXfma_3_1 
_ImonXfch_2_1 _ImonXfch_3_1 
_ImonXmara2_1 _ImonXmara3_1 
_ImonXfmaa2_1 _ImonXfmaa3_1 if soccode>0, vce(robust);
# delimit cr






*men, single w/o children
lincom _b[_Imonth_2] 
local se11=r(se)
local coef11=r(estimate)

lincom _b[_Imonth_3] 
local se12=r(se)
local coef12=r(estimate)



*men, single w/ children
lincom _b[_Imonth_2] + _b[_ImonXchi_2_1]
local se21=r(se)
local coef21=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXchi_3_1]
local se22=r(se)
local coef22=r(estimate)




*men, married w/o children
lincom _b[_Imonth_2] + _b[_ImonXmar_2_1]
local se31=r(se)
local coef31=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXmar_3_1]
local se32=r(se)
local coef32=r(estimate)




*men, married w/ children
lincom _b[_Imonth_2] + _b[_ImonXmar_2_1] +_b[_ImonXchi_2_1] +_b[ _ImonXmara2_1]
local se41=r(se)
local coef41=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXmar_3_1] +_b[_ImonXchi_3_1] +_b[ _ImonXmara3_1]
local se42=r(se)
local coef42=r(estimate)




*women, single w/o children
lincom _b[_Imonth_2] + _b[_ImonXfem_2_1]
local se51=r(se)
local coef51=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXfem_3_1]
local se52=r(se)
local coef52=r(estimate)




*women, single w/ children
lincom _b[_Imonth_2] + _b[_ImonXchi_2_1]+ _b[_ImonXfem_2_1] + _b[_ImonXfch_2_1]
local se61=r(se)
local coef61=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXchi_3_1]+ _b[_ImonXfem_3_1] + _b[_ImonXfch_3_1]
local se62=r(se)
local coef62=r(estimate)




*women, married w/o children
lincom _b[_Imonth_2] + _b[_ImonXmar_2_1]+ _b[_ImonXfem_2_1] + _b[_ImonXfma_2_1]
local se71=r(se)
local coef71=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXmar_3_1]+ _b[_ImonXfem_3_1] + _b[_ImonXfma_3_1]
local se72=r(se)
local coef72=r(estimate)




*women, married w/ children
lincom _b[_Imonth_2] + _b[_ImonXmar_2_1] +_b[_ImonXchi_2_1] +_b[ _ImonXmara2_1]+ _b[_ImonXfem_2_1] + _b[_ImonXfmaa2_1]+ _b[_ImonXfma_2_1]+ _b[_ImonXfch_2_1]
local se81=r(se)
local coef81=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXmar_3_1] +_b[_ImonXchi_3_1] +_b[ _ImonXmara3_1]+ _b[_ImonXfem_3_1] + _b[_ImonXfmaa3_1]+ _b[_ImonXfma_3_1]+ _b[_ImonXfch_3_1]
local se82=r(se)
local coef82=r(estimate)



*diff, single w/o children
lincom (_b[_Imonth_2] + _b[_ImonXfem_2_1]) - _b[_Imonth_2]
local se91=r(se)
local coef91=r(estimate)

lincom (_b[_Imonth_3] + _b[_ImonXfem_3_1]) - _b[_Imonth_3]
local se92=r(se)
local coef92=r(estimate)




*diff, single w/ children
lincom _b[_ImonXfem_2_1] + _b[_ImonXfch_2_1]
local se101=r(se)
local coef101=r(estimate)

lincom _b[_ImonXfem_3_1] + _b[_ImonXfch_3_1]
local se102=r(se)
local coef102=r(estimate)




*diff, married w/o children
lincom _b[_ImonXfem_2_1] + _b[_ImonXfma_2_1]
local se111=r(se)
local coef111=r(estimate)

lincom _b[_ImonXfem_3_1] + _b[_ImonXfma_3_1]
local se112=r(se)
local coef112=r(estimate)




*diff, married w/ children
lincom _b[_ImonXfem_2_1] + _b[_ImonXfmaa2_1]+ _b[_ImonXfma_2_1]+ _b[_ImonXfch_2_1]
local se121=r(se)
local coef121=r(estimate)

lincom _b[_ImonXfem_3_1] + _b[_ImonXfmaa3_1]+ _b[_ImonXfma_3_1]+ _b[_ImonXfch_3_1]
local se122=r(se)
local coef122=r(estimate)





# delimit ;
outreg2 using "${result}\Corona_Noocc", append excel noaster addstat("coef_singlemenwithoutchildren_recession",`coef11',"coef_singlemenwithoutchildren_recovery",`coef12',
"coef_singlemenwithchildren_recession",`coef21',"coef_singlemenwithchildren_recovery",`coef22',
"coef_marriedmenwithoutchildren_recession",`coef31',"coef_marriedmenwithoutchildren_recovery",`coef32',
"coef_marriedmenwithchildren_recession",`coef41',"coef_marriedmenwithchildren_recovery",`coef42',
"coef_singlewomenwithoutchildren_recession",`coef51',"coef_singlewomenwithoutchildren_recovery",`coef52',
"coef_singlewomenwithchildren_recession",`coef61',"coef_singlewomenwithchildren_recovery",`coef62',
"coef_marriedwomenwithoutchildren_recession",`coef71',"coef_marriedwomenwithoutchildren_recovery",`coef72',
"coef_marriedwomenwithchildren_recession",`coef81',"coef_marriedwomenwithchildren_recovery",`coef82',
"diff_singlewithoutchildren_recession",`coef91',"diff_singlewithoutchildren_recovery",`coef92',
"diff_singlewomenwithchildren_recession",`coef101',"diff_singlewithchildren_recovery",`coef102',
"diff_marriedwomenwithoutchildren_recession",`coef111',"diff_marriedwithoutchildren_recovery",`coef112',
"diff_marriedwomenwithchildren_recession",`coef121',"diff_marriedwithchildren_recovery",`coef122',
"se_singlemenwithoutchildren_recession",`se11',"se_singlemenwithoutchildren_recovery",`se12',
"se_singlemenwithchildren_recession",`se21',"se_singlemenwithchildren_recovery",`se22',
"se_marriedmenwithoutchildren_recession",`se31',"se_marriedmenwithoutchildren_recovery",`se32',
"se_marriedmenwithchildren_recession",`se41',"se_marriedmenwithchildren_recovery",`se42',
"se_singlewomenwithoutchildren_recession",`se51',"se_singlewomenwithoutchildren_recovery",`se52',
"se_singlewomenwithchildren_recession",`se61',"se_singlewomenwithchildren_recovery",`se62',
"se_marriedwomenwithoutchildren_recession",`se71',"se_marriedwomenwithoutchildren_recovery",`se72',
"se_marriedwomenwithchildren_recession",`se81',"se_marriedwomenwithchildren_recovery",`se82',
"diffse_singlewithoutchildren_recession",`se91',"diffse_singlewithoutchildren_recovery",`se92',
"diffse_singlewomenwithchildren_recession",`se101',"diffse_singlewithchildren_recovery",`se102',
"diffse_marriedwomenwithoutchildren_recession",`se111',"diffse_marriedwithoutchildren_recovery",`se112',
"diffse_marriedwomenwithchildren_recession",`se121',"diffse_marriedwithchildren_recovery",`se122');
# delimit cr

local drop _all

}














********************************************************************************
* Stock regressions
********************************************************************************
foreach dvar in employment unemp nilf{

use "${proc}/data_for_stock.dta", clear

# delimit ;
reg `dvar' _Isoccode_13 _Isoccode_15 _Isoccode_17 _Isoccode_19 _Isoccode_21 _Isoccode_23 _Isoccode_25 
_Isoccode_27 _Isoccode_29 _Isoccode_31 _Isoccode_33 _Isoccode_35 _Isoccode_37 _Isoccode_39 _Isoccode_41 
_Isoccode_43 _Isoccode_45 _Isoccode_47 _Isoccode_49 _Isoccode_51 _Isoccode_53 
_ImonXsoc_2_13 _ImonXsoc_2_15 _ImonXsoc_2_17 _ImonXsoc_2_19 _ImonXsoc_2_21 _ImonXsoc_2_23 _ImonXsoc_2_25 _ImonXsoc_2_27 _ImonXsoc_2_29 
_ImonXsoc_2_31 _ImonXsoc_2_33 _ImonXsoc_2_35 _ImonXsoc_2_37 _ImonXsoc_2_39 _ImonXsoc_2_41 _ImonXsoc_2_43 _ImonXsoc_2_45 _ImonXsoc_2_47 
_ImonXsoc_2_49 _ImonXsoc_2_51 _ImonXsoc_2_53 
_ImonXsoc_3_13 _ImonXsoc_3_15 _ImonXsoc_3_17 _ImonXsoc_3_19 _ImonXsoc_3_21 _ImonXsoc_3_23 _ImonXsoc_3_25 _ImonXsoc_3_27 _ImonXsoc_3_29 
_ImonXsoc_3_31 _ImonXsoc_3_33 _ImonXsoc_3_35 _ImonXsoc_3_37 _ImonXsoc_3_39 _ImonXsoc_3_41 _ImonXsoc_3_43 _ImonXsoc_3_45 _ImonXsoc_3_47 
_ImonXsoc_3_49 _ImonXsoc_3_51 _ImonXsoc_3_53 _Iagegroup_1 _Iagegroup_3 _ImonXage_2_1 _ImonXage_2_3 _ImonXage_3_1 _ImonXage_3_3 
_Iedu2_1 _Iedu2_2 _ImonXedu_2_1 _ImonXedu_2_2 _ImonXedu_3_1 _ImonXedu_3_2 _Imonth_2 _Imonth_3 
_Ifemale_1 _Imarried_1 _Ichildren_1 _Ifmar_1 _Ifchd_1 _Imarchd_1 _Ifmarchd_1 _ImonXfem_2_1 _ImonXfem_3_1 
_ImonXmar_2_1 _ImonXmar_3_1 _ImonXchi_2_1 _ImonXchi_3_1 _ImonXfma_2_1 _ImonXfma_3_1 
_ImonXfch_2_1 _ImonXfch_3_1 _ImonXmara2_1 _ImonXmara3_1 _ImonXfmaa2_1 _ImonXfmaa3_1 [pweight=wtfinl] if soccode>0, vce(robust);
# delimit cr 



*men, single w/o children
lincom _b[_Imonth_2] 
local se11=r(se)
local coef11=r(estimate)

lincom _b[_Imonth_3] 
local se12=r(se)
local coef12=r(estimate)



*men, single w/ children
lincom _b[_Imonth_2] + _b[_ImonXchi_2_1]
local se21=r(se)
local coef21=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXchi_3_1]
local se22=r(se)
local coef22=r(estimate)




*men, married w/o children
lincom _b[_Imonth_2] + _b[_ImonXmar_2_1]
local se31=r(se)
local coef31=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXmar_3_1]
local se32=r(se)
local coef32=r(estimate)




*men, married w/ children
lincom _b[_Imonth_2] + _b[_ImonXmar_2_1] +_b[_ImonXchi_2_1] +_b[ _ImonXmara2_1]
local se41=r(se)
local coef41=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXmar_3_1] +_b[_ImonXchi_3_1] +_b[ _ImonXmara3_1]
local se42=r(se)
local coef42=r(estimate)




*women, single w/o children
lincom _b[_Imonth_2] + _b[_ImonXfem_2_1]
local se51=r(se)
local coef51=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXfem_3_1]
local se52=r(se)
local coef52=r(estimate)




*women, single w/ children
lincom _b[_Imonth_2] + _b[_ImonXchi_2_1]+ _b[_ImonXfem_2_1] + _b[_ImonXfch_2_1]
local se61=r(se)
local coef61=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXchi_3_1]+ _b[_ImonXfem_3_1] + _b[_ImonXfch_3_1]
local se62=r(se)
local coef62=r(estimate)




*women, married w/o children
lincom _b[_Imonth_2] + _b[_ImonXmar_2_1]+ _b[_ImonXfem_2_1] + _b[_ImonXfma_2_1]
local se71=r(se)
local coef71=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXmar_3_1]+ _b[_ImonXfem_3_1] + _b[_ImonXfma_3_1]
local se72=r(se)
local coef72=r(estimate)




*women, married w/ children
lincom _b[_Imonth_2] + _b[_ImonXmar_2_1] +_b[_ImonXchi_2_1] +_b[ _ImonXmara2_1]+ _b[_ImonXfem_2_1] + _b[_ImonXfmaa2_1]+ _b[_ImonXfma_2_1]+ _b[_ImonXfch_2_1]
local se81=r(se)
local coef81=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXmar_3_1] +_b[_ImonXchi_3_1] +_b[ _ImonXmara3_1]+ _b[_ImonXfem_3_1] + _b[_ImonXfmaa3_1]+ _b[_ImonXfma_3_1]+ _b[_ImonXfch_3_1]
local se82=r(se)
local coef82=r(estimate)


*diff, single w/o children
lincom (_b[_Imonth_2] + _b[_ImonXfem_2_1]) - _b[_Imonth_2]
local se91=r(se)
local coef91=r(estimate)

lincom (_b[_Imonth_3] + _b[_ImonXfem_3_1]) - _b[_Imonth_3]
local se92=r(se)
local coef92=r(estimate)




*diff, single w/ children
lincom _b[_ImonXfem_2_1] + _b[_ImonXfch_2_1]
local se101=r(se)
local coef101=r(estimate)

lincom _b[_ImonXfem_3_1] + _b[_ImonXfch_3_1]
local se102=r(se)
local coef102=r(estimate)




*diff, married w/o children
lincom _b[_ImonXfem_2_1] + _b[_ImonXfma_2_1]
local se111=r(se)
local coef111=r(estimate)

lincom _b[_ImonXfem_3_1] + _b[_ImonXfma_3_1]
local se112=r(se)
local coef112=r(estimate)




*diff, married w/ children
lincom _b[_ImonXfem_2_1] + _b[_ImonXfmaa2_1]+ _b[_ImonXfma_2_1]+ _b[_ImonXfch_2_1]
local se121=r(se)
local coef121=r(estimate)

lincom _b[_ImonXfem_3_1] + _b[_ImonXfmaa3_1]+ _b[_ImonXfma_3_1]+ _b[_ImonXfch_3_1]
local se122=r(se)
local coef122=r(estimate)




# delimit ;
outreg2 using "${result}\Corona_2digitocc", append excel noaster addstat("coef_singlemenwithoutchildren_recession",`coef11',"coef_singlemenwithoutchildren_recovery",`coef12',
"coef_singlemenwithchildren_recession",`coef21',"coef_singlemenwithchildren_recovery",`coef22',
"coef_marriedmenwithoutchildren_recession",`coef31',"coef_marriedmenwithoutchildren_recovery",`coef32',
"coef_marriedmenwithchildren_recession",`coef41',"coef_marriedmenwithchildren_recovery",`coef42',
"coef_singlewomenwithoutchildren_recession",`coef51',"coef_singlewomenwithoutchildren_recovery",`coef52',
"coef_singlewomenwithchildren_recession",`coef61',"coef_singlewomenwithchildren_recovery",`coef62',
"coef_marriedwomenwithoutchildren_recession",`coef71',"coef_marriedwomenwithoutchildren_recovery",`coef72',
"coef_marriedwomenwithchildren_recession",`coef81',"coef_marriedwomenwithchildren_recovery",`coef82',
"diff_singlewithoutchildren_recession",`coef91',"diff_singlewithoutchildren_recovery",`coef92',
"diff_singlewomenwithchildren_recession",`coef101',"diff_singlewithchildren_recovery",`coef102',
"diff_marriedwomenwithoutchildren_recession",`coef111',"diff_marriedwithoutchildren_recovery",`coef112',
"diff_marriedwomenwithchildren_recession",`coef121',"diff_marriedwithchildren_recovery",`coef122',
"se_singlemenwithoutchildren_recession",`se11',"se_singlemenwithoutchildren_recovery",`se12',
"se_singlemenwithchildren_recession",`se21',"se_singlemenwithchildren_recovery",`se22',
"se_marriedmenwithoutchildren_recession",`se31',"se_marriedmenwithoutchildren_recovery",`se32',
"se_marriedmenwithchildren_recession",`se41',"se_marriedmenwithchildren_recovery",`se42',
"se_singlewomenwithoutchildren_recession",`se51',"se_singlewomenwithoutchildren_recovery",`se52',
"se_singlewomenwithchildren_recession",`se61',"se_singlewomenwithchildren_recovery",`se62',
"se_marriedwomenwithoutchildren_recession",`se71',"se_marriedwomenwithoutchildren_recovery",`se72',
"se_marriedwomenwithchildren_recession",`se81',"se_marriedwomenwithchildren_recovery",`se82',
"diffse_singlewithoutchildren_recession",`se91',"diffse_singlewithoutchildren_recovery",`se92',
"diffse_singlewomenwithchildren_recession",`se101',"diffse_singlewithchildren_recovery",`se102',
"diffse_marriedwomenwithoutchildren_recession",`se111',"diffse_marriedwithoutchildren_recovery",`se112',
"diffse_marriedwomenwithchildren_recession",`se121',"diffse_marriedwithchildren_recovery",`se122');
# delimit cr

local drop se*



* No occupation controls

# delimit ;
reg `dvar' _Iagegroup_1 _Iagegroup_3 _ImonXage_2_1 _ImonXage_2_3 _ImonXage_3_1 _ImonXage_3_3 
_Iedu2_1 _Iedu2_2 _ImonXedu_2_1 _ImonXedu_2_2 _ImonXedu_3_1 _ImonXedu_3_2 
_Imonth_2 _Imonth_3 
_Ifemale_1 _Imarried_1 _Ichildren_1 _Ifmar_1 _Ifchd_1 _Imarchd_1 _Ifmarchd_1 
_ImonXfem_2_1 _ImonXfem_3_1 
_ImonXmar_2_1 _ImonXmar_3_1 
_ImonXchi_2_1 _ImonXchi_3_1 
_ImonXfma_2_1 _ImonXfma_3_1 
_ImonXfch_2_1 _ImonXfch_3_1 
_ImonXmara2_1 _ImonXmara3_1 
_ImonXfmaa2_1 _ImonXfmaa3_1 if soccode>0, vce(robust);
# delimit cr

*men, single w/o children
lincom _b[_Imonth_2] 
local se11=r(se)
local coef11=r(estimate)

lincom _b[_Imonth_3] 
local se12=r(se)
local coef12=r(estimate)




*men, single w/ children
lincom _b[_Imonth_2] + _b[_ImonXchi_2_1]
local se21=r(se)
local coef21=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXchi_3_1]
local se22=r(se)
local coef22=r(estimate)




*men, married w/o children
lincom _b[_Imonth_2] + _b[_ImonXmar_2_1]
local se31=r(se)
local coef31=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXmar_3_1]
local se32=r(se)
local coef32=r(estimate)




*men, married w/ children
lincom _b[_Imonth_2] + _b[_ImonXmar_2_1] +_b[_ImonXchi_2_1] +_b[ _ImonXmara2_1]
local se41=r(se)
local coef41=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXmar_3_1] +_b[_ImonXchi_3_1] +_b[ _ImonXmara3_1]
local se42=r(se)
local coef42=r(estimate)




*women, single w/o children
lincom _b[_Imonth_2] + _b[_ImonXfem_2_1]
local se51=r(se)
local coef51=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXfem_3_1]
local se52=r(se)
local coef52=r(estimate)



*women, single w/ children
lincom _b[_Imonth_2] + _b[_ImonXchi_2_1]+ _b[_ImonXfem_2_1] + _b[_ImonXfch_2_1]
local se61=r(se)
local coef61=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXchi_3_1]+ _b[_ImonXfem_3_1] + _b[_ImonXfch_3_1]
local se62=r(se)
local coef62=r(estimate)




*women, married w/o children
lincom _b[_Imonth_2] + _b[_ImonXmar_2_1]+ _b[_ImonXfem_2_1] + _b[_ImonXfma_2_1]
local se71=r(se)
local coef71=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXmar_3_1]+ _b[_ImonXfem_3_1] + _b[_ImonXfma_3_1]
local se72=r(se)
local coef72=r(estimate)




*women, married w/ children
lincom _b[_Imonth_2] + _b[_ImonXmar_2_1] +_b[_ImonXchi_2_1] +_b[ _ImonXmara2_1]+ _b[_ImonXfem_2_1] + _b[_ImonXfmaa2_1]+ _b[_ImonXfma_2_1]+ _b[_ImonXfch_2_1]
local se81=r(se)
local coef81=r(estimate)

lincom _b[_Imonth_3] + _b[_ImonXmar_3_1] +_b[_ImonXchi_3_1] +_b[ _ImonXmara3_1]+ _b[_ImonXfem_3_1] + _b[_ImonXfmaa3_1]+ _b[_ImonXfma_3_1]+ _b[_ImonXfch_3_1]
local se82=r(se)
local coef82=r(estimate)



*diff, single w/o children
lincom (_b[_Imonth_2] + _b[_ImonXfem_2_1]) - _b[_Imonth_2]
local se91=r(se)
local coef91=r(estimate)

lincom (_b[_Imonth_3] + _b[_ImonXfem_3_1]) - _b[_Imonth_3]
local se92=r(se)
local coef92=r(estimate)




*diff, single w/ children
lincom _b[_ImonXfem_2_1] + _b[_ImonXfch_2_1]
local se101=r(se)
local coef101=r(estimate)

lincom _b[_ImonXfem_3_1] + _b[_ImonXfch_3_1]
local se102=r(se)
local coef102=r(estimate)




*diff, married w/o children
lincom _b[_ImonXfem_2_1] + _b[_ImonXfma_2_1]
local se111=r(se)
local coef111=r(estimate)

lincom _b[_ImonXfem_3_1] + _b[_ImonXfma_3_1]
local se112=r(se)
local coef112=r(estimate)




*diff, married w/ children
lincom _b[_ImonXfem_2_1] + _b[_ImonXfmaa2_1]+ _b[_ImonXfma_2_1]+ _b[_ImonXfch_2_1]
local se121=r(se)
local coef121=r(estimate)

lincom _b[_ImonXfem_3_1] + _b[_ImonXfmaa3_1]+ _b[_ImonXfma_3_1]+ _b[_ImonXfch_3_1]
local se122=r(se)
local coef122=r(estimate)




# delimit ;
outreg2 using "${result}\Corona_Noocc", append excel noaster addstat("coef_singlemenwithoutchildren_recession",`coef11',"coef_singlemenwithoutchildren_recovery",`coef12',
"coef_singlemenwithchildren_recession",`coef21',"coef_singlemenwithchildren_recovery",`coef22',
"coef_marriedmenwithoutchildren_recession",`coef31',"coef_marriedmenwithoutchildren_recovery",`coef32',
"coef_marriedmenwithchildren_recession",`coef41',"coef_marriedmenwithchildren_recovery",`coef42',
"coef_singlewomenwithoutchildren_recession",`coef51',"coef_singlewomenwithoutchildren_recovery",`coef52',
"coef_singlewomenwithchildren_recession",`coef61',"coef_singlewomenwithchildren_recovery",`coef62',
"coef_marriedwomenwithoutchildren_recession",`coef71',"coef_marriedwomenwithoutchildren_recovery",`coef72',
"coef_marriedwomenwithchildren_recession",`coef81',"coef_marriedwomenwithchildren_recovery",`coef82',
"diff_singlewithoutchildren_recession",`coef91',"diff_singlewithoutchildren_recovery",`coef92',
"diff_singlewomenwithchildren_recession",`coef101',"diff_singlewithchildren_recovery",`coef102',
"diff_marriedwomenwithoutchildren_recession",`coef111',"diff_marriedwithoutchildren_recovery",`coef112',
"diff_marriedwomenwithchildren_recession",`coef121',"diff_marriedwithchildren_recovery",`coef122',
"se_singlemenwithoutchildren_recession",`se11',"se_singlemenwithoutchildren_recovery",`se12',
"se_singlemenwithchildren_recession",`se21',"se_singlemenwithchildren_recovery",`se22',
"se_marriedmenwithoutchildren_recession",`se31',"se_marriedmenwithoutchildren_recovery",`se32',
"se_marriedmenwithchildren_recession",`se41',"se_marriedmenwithchildren_recovery",`se42',
"se_singlewomenwithoutchildren_recession",`se51',"se_singlewomenwithoutchildren_recovery",`se52',
"se_singlewomenwithchildren_recession",`se61',"se_singlewomenwithchildren_recovery",`se62',
"se_marriedwomenwithoutchildren_recession",`se71',"se_marriedwomenwithoutchildren_recovery",`se72',
"se_marriedwomenwithchildren_recession",`se81',"se_marriedwomenwithchildren_recovery",`se82',
"diffse_singlewithoutchildren_recession",`se91',"diffse_singlewithoutchildren_recovery",`se92',
"diffse_singlewomenwithchildren_recession",`se101',"diffse_singlewithchildren_recovery",`se102',
"diffse_marriedwomenwithoutchildren_recession",`se111',"diffse_marriedwithoutchildren_recovery",`se112',
"diffse_marriedwomenwithchildren_recession",`se121',"diffse_marriedwithchildren_recovery",`se122');
# delimit cr

local drop _all

}






