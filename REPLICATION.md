# [JEP-2021-1217] [The Different Effects of the COVID-19 Recession on the US Labor Market: Occupation, Family and Gender] Validation and Replication results

> Some useful links:
> - [Official Data and Code Availability Policy](https://www.aeaweb.org/journals/policies/data-code)
> - [Step by step guidance](https://aeadataeditor.github.io/aea-de-guidance/) 
> - [Template README](https://social-science-data-editors.github.io/template_README/)

SUMMARY
-------

 Thank you for your replication archive. Most tables and figures are reproduced successfully. In assessing compliance with our [Data and Code Availability Policy](https://www.aeaweb.org/journals/policies/data-code), we have identified the following issues, which we ask you to address: discrepancies between output and the paper version, incomplete description of access modality, incomplete code, mapping, and setup program, missing data citations.

 The issues that need to be addressed can be addressed with relatively little effort. We thus accept the replication package subject to the modifications outlined below.

**Conditional on making the requested changes to the manuscript and the openICPSR deposit prior to publication, the replication package is accepted.**

### Action items (manuscript)

- [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html#confidential-databases).

> [REQUIRED] Please adjust your tables to account for the noted numerical discrepancies, or explain (in the README) discrepancies that a replicator should expect. 

### Action items (openICPSR)

- [REQUIRED] Please provide a clear description of access modality and source location for this dataset. Some examples are given [on this website](https://social-science-data-editors.github.io/guidance/Requested_information_dcas.html).

- [REQUIRED] Please provide a mapping of programs to Table 10, 11, and 12, or add comments to the code that identify where Table 10, 11, and 12 is produced.

- [REQUIRED] Please provide complete code, including for appendix tables 10-12. 

- [REQUIRED] Please amend README to contain complete requirements. 

- [REQUIRED] Please add a setup program that installs all Stata packages. Please specify all necessary commands. An example of a setup file can be found at [https://github.com/gslab-econ/template/blob/master/config/config_stata.do](https://github.com/gslab-econ/template/blob/master/config/config_stata.do)

> The openICPSR submission process has changed. If you have not already done so, please "Change Status -> Submit to AEA" from your deposit Workspace.


General
-------

> [SUGGESTED] A recommended README template for replication packages in economics can be found on the [Social Science Data Editor Github site](https://social-science-data-editors.github.io/guidance/template-README.html).

Data description
----------------

### Data Sources

#### Current Population Survey (CPS) 

- Dataset is provided
  - Provided datasets: cps_00073.dta, cps_00074.dta, cps_00075.dta
- Access conditions are not described, nor a link to a data landing page provided.
- The data are not cited in the paper nor in the README.
  - The file names look like they might stem from IPUMS. Please refer to your (annual) data use agreement at IPUMS for citation requirements.

> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html#confidential-databases).

> [REQUIRED] Please provide a clear description of access modality and source location for this dataset. Some examples are given [on this website](https://social-science-data-editors.github.io/guidance/Requested_information_dcas.html). 

#### Occupational Information Network (O*NET)

- Dataset is provided
  - Provided datasets: Work Activities.txt, Work Context.txt, occlist.dta, crosswalk.dta
- Access conditions are not described, nor a link to a data landing page provided.
- The data are not cited in the paper nor in the README.

> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html#confidential-databases).

> [REQUIRED] Please provide a clear description of access modality and source location for this dataset. Some examples are given [on this website](https://social-science-data-editors.github.io/guidance/Requested_information_dcas.html). 

O*NET updates data frequently, and provides precisely specified versions. Please make sure to cite the version you used (and provided).

#### Occupation codes from David Dorn webpage

- Dataset is provided.
  - Provided dataset: occ1990dd_task_alm.dta
- Access conditions are not described, nor a link to a data landing page provided.
- Following paper is cited in the paper, but not the dataset.

  > Autor, David H., and David Dorn. 2013. "The Growth of Low-Skill Service Jobs and the Polarization of the US Labor Market." American Economic Review 103 (5): 1553-97 (August).

> [REQUIRED] Please add **data** citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html#confidential-databases).

> [REQUIRED] Please provide a clear description of access modality and source location for this dataset. Some examples are given [on this website](https://social-science-data-editors.github.io/guidance/Requested_information_dcas.html). 

#### crosswalk between the 1990 and 2010 Census Bureau occupational classification schemes.

- Dataset is provided: "occ2010_occ1990.dta"
- Access conditions are not described, nor a link to a data landing page provided.
- The data are not cited in the paper nor in the README.
  - If these come from David Dorn (as suspected), then make that clear in the README.

> [REQUIRED] Please add data citations to the article. Guidance on how to cite data is provided in the [AEA Sample References](https://www.aeaweb.org/journals/policies/sample-references) and in [additional guidance](https://social-science-data-editors.github.io/guidance/addtl-data-citation-guidance.html#confidential-databases).

> [REQUIRED] Please provide a clear description of access modality and source location for this dataset. Some examples are given [on this website](https://social-science-data-editors.github.io/guidance/Requested_information_dcas.html). 


### Analysis Data Files

- [ ] No analysis data file mentioned
- [ ] Analysis data files mentioned, not provided (explain reasons below)
- [X] Analysis data files mentioned, provided. File names listed below.

- classification.xlsx

Data deposit
------------

### Requirements 

- [X] README is in TXT, MD, PDF format
- [X] openICPSR deposit has no ZIP files
- [ ] Title conforms to guidance (starts with "Data and Code for:" or "Code for:", is properly capitalized)
- [ ] Authors (with affiliations) are listed in the same order as on the paper

> [REQUIRED] Please review the title of the openICPSR deposit as per our guidelines (below).

> [REQUIRED] Please review authors and affiliations on the openICPSR deposit. In general, they are the same, and in the same order, as for the manuscript; however, authors can deviate from that order.


Data checks
-----------

- Data can be read using Stata
- Two datasets in archive-ready formats(txt)
    - workactivities
    - workcontext
  - Eight are in custom formats(dta)
- Data files have variable labels
- PII scan identified 36 variables with potential issues with personal identifiers, but all of them are false positives.


Code description
----------------

There are ten provided Stata do files, including a `master.do` and five Matlab files to generate figures.

Most Tables and figures are mapped to code in the README, except for Table 10, 11, and 12.

| Data Preparation Program      | Dataset created                                                                               |
|-------------------------------|-----------------------------------------------------------------------------------------------|
| generate_data_for_stock.do    | data_for_stock.dta                                                                            |
| generate_data_for_stock_gc.do | data_for_stock_gc.dta                                                                         |
| generate_data_for_flow.do     | data_for_flow.dta                                                                             |
| generate_data_for_flow_gc.do  | data_for_flow_gc.dta                                                                          |
| generate_Epseries.do          | EPseries_monthly_sa_all.xlsx, EPseries_monthly_15andolder.xlsx, CPS_COVID_EMPLOYMENT_NEW.xlsx |

| Figure/Table # | Program                                                        | Line Number |
|----------------|----------------------------------------------------------------|-------------|
| table 1        | ONET_analysis.do                                               | 157-175     |
| table 2        | generate_data_for_stock.do                                     | 158-206     |
| table 3        | make_regression_charts.m                                       |             |
| table 4        | make_regression_charts.m                                       |             |
| table 5        | generate_data_for_stock.do                                     | 158-206     |
| table 6        | ONET_analysis.do                                               | 133-155     |
| table 7        | generate_data_for_stock_gc.do                                  | 167-207     |
| table 8        | make_regression_charts.m                                       |             |
| table 9        | generate_data_for_stock.do <br/>generate_data_for_stock_gc.do  |             |
| table 10       | Not provided                                                   |             |
| table 11       | Not provided                                                   |             |
| table 12       | Not provided                                                   |             |
| figure 1       | make_figure_1                                                  |             |
| figure 2       | make_figure_2                                                  |             |
| figure 3       | make_figure_3                                                  |             |
| figure 4       | make_regression_charts                                         |             |
| figure 5       | make_regression_charts                                         |             |
| figure 6       | make_regression_charts                                         |             |
| figure 7       | make_figure_7                                                  |             |
| figure 8       | make_regression_charts                                         |             |
| figure 9       | make_regression_charts                                         |

> [REQUIRED] Please provide a mapping of programs to Table 10, 11, and 12, or add comments to the code that identify where Table 10, 11, and 12 is produced.

> [REQUIRED] Please provide complete code, including for appendix tables 10-12. 

Stated Requirements
---------------------

- [X] No requirements specified


Missing Requirements
--------------------
- [X] Software Requirements 
  - [X] Stata
    - [X] Version
    - Packages go here
      - `outreg2, egenmisc, tabout`
  - [X] Matlab
    - [X] Version
  - [X] Computational Requirements specified as follows:
  - Cluster size, disk size, memory size, etc.
- [X] Time Requirements 
  - Length of necessary computation (hours, weeks, etc.)

> [REQUIRED] Please amend README to contain complete requirements. 

Computing Environment of the Replicator
---------------------

- CISER Shared Windows Server 2019, 256GB, Intel Xeon E5-4669 v3 @ 2.10Ghz (2 processors, 36 cores)
- Stata/MP 16.1
- Matlab R2020a

Replication steps
-----------------

1. Downloaded code from openICPSR.
2. Downloaded data from URL provided. No access conditions needed to be met.
3. Ran code as per README: the README only states to run the master do file
4. Ran with errors
  - error occured in `ONET_analysis.do` file
    - Line 110, 155, 175: file already exists
      - added replace to the "save "${temp}/workcontext"
    - tried to install correct packages but did not work, error read "unknown egen function wtmean()"
5. In order to produce the figures, the matlab files within the "figures" folder needed to be run as well which is not included in the README and should be added.

> [REQUIRED] Please add a setup program that installs all Stata packages. Please specify all necessary commands. An example of a setup file can be found at [https://github.com/gslab-econ/template/blob/master/config/config_stata.do](https://github.com/gslab-econ/template/blob/master/config/config_stata.do)

> [REQUIRED] Please ensure that the README specifies all necessary steps.


Findings
--------
### Data Preparation Code
| Data Preparation Program      | Dataset created                                                                               |  | reproduced? |
|-------------------------------|-----------------------------------------------------------------------------------------------|--|-------------|
| generate_data_for_stock.do    | data_for_stock.dta                                                                            |  | Yes         |
| generate_data_for_stock_gc.do | data_for_stock_gc.dta                                                                         |  | Yes         |
| generate_data_for_flow.do     | data_for_flow.dta                                                                             |  | Yes         |
| generate_data_for_flow_gc.do  | data_for_flow_gc.dta                                                                          |  | Yes         |
| generate_Epseries.do          | EPseries_monthly_sa_all.xlsx, EPseries_monthly_15andolder.xlsx, CPS_COVID_EMPLOYMENT_NEW.xlsx |  | Yes         |

### Tables
| Table # | Program                                                        | Line Number | reproduced? | Note                                                     |
|----------------|----------------------------------------------------------------|-------------|-------------|----------------------------------------------------------|
| table 1        | ONET_analysis.do                                               | 157-175     | Yes         |                                                          |
| table 2        | generate_data_for_stock.do                                     | 158-206     | Yes         |                                                          |
| table 3        | make_regression_charts.m                                       |             | Yes         |                                                          |
| table 4        | make_regression_charts.m                                       |             | Yes         |                                                          |
| table 5        | generate_data_for_stock.do                                     | 158-206     | Yes         |                                                          |
| table 6        | ONET_analysis.do                                               | 133-155     | No          | Output provides integer, whereas the paper uses decimal. |
| table 7        | generate_data_for_stock_gc.do                                  | 167-207     | Yes         |                                                          |
| table 8        | make_regression_charts.m                                       |             | Yes         |                                                          |
| table 9        | generate_data_for_stock.do <br/>generate_data_for_stock_gc.do  |             | Yes         |                                                          |
| table 10       | Not provided                                                   |             |             |                                                          |
| table 11       | Not provided                                                   |             |             |                                                          |
| table 12       | Not provided                                                   |

### Figures
| Figure # | Program                                                        | Line Number | reproduced? | Note                                                     |
|----------------|----------------------------------------------------------------|-------------|-------------|----------------------------------------------------------|
| figure 1       | make_figure_1                                                  |             | Yes         |                                                          |
| figure 2       | make_figure_2                                                  |             | No          | Legends and labels differ                                |
| figure 3       | make_figure_3                                                  |             | No          | Graph size differs so not able to compare                |
| figure 4       | make_regression_charts                                         |             | Yes         |                                                          |
| figure 5       | make_regression_charts                                         |             | Yes         |                                                          |
| figure 6       | make_regression_charts                                         |             | Yes         |                                                          |
| figure 7       | make_figure_7                                                  |             | No          | Panel (b) differs                                        |
| figure 8       | make_regression_charts                                         |             | Yes         |                                                          |
| figure 9       | make_regression_charts                                         |             | Yes         |

- Figure 2: Legends and labels differ from the paper version
  - Paper version:
    ![Paper version](screenshot/fig2a_paper.png)

  - Figure 2a generated by programs:
    ![reproduced version](139962/Figures/Figure 2/EoverP_barchart_overlay_GR.png)

- Figure 3: Graph size differs
  - Paper version:
    ![Paper version](screenshot/fig3_paper.png)

  - Figure 3a generated by programs:
    ![reproduced version](139962/Figures/Figure3/ep_by_occgender_agg_yoy.png)

- Figure 7: 
  - Paper version:
    ![Paper version](screenshot/fig7b_paper.png)

  - Figure 3a generated by programs:
    ![reproduced version](139962/Figures/Figure7/e_corr_0_byocc.png)

> [REQUIRED] Please adjust your tables to account for the noted numerical discrepancies, or explain (in the README) discrepancies that a replicator should expect. 

### In-Text Numbers

- [x] There are no in-text numbers, or all in-text numbers stem from tables and figures.

- [ ] There are in-text numbers, but they are not identified in the code


Classification
--------------

- [ ] full reproduction
- [x] full reproduction with minor issues
- [ ] partial reproduction (see above)
- [ ] not able to reproduce most or all of the results (reasons see above)

### Reason for incomplete reproducibility

- [x] `Discrepancy in output` (either figures or numbers in tables or text differ)
- [x] `Bugs in code`  that  were fixable by the replicator (but should be fixed in the final deposit)
- [ ] `Code missing`, in particular if it  prevented the replicator from completing the reproducibility check
  - [ ] `Data preparation code missing` should be checked if the code missing seems to be data preparation code
- [ ] `Code not functional` is more severe than a simple bug: it  prevented the replicator from completing the reproducibility check
- [ ] `Software not available to replicator`  may happen for a variety of reasons, but in particular (a) when the software is commercial, and the replicator does not have access to a licensed copy, or (b) the software is open-source, but a specific version required to conduct the reproducibility check is not available.
- [ ] `Insufficient time available to replicator` is applicable when (a) running the code would take weeks or more (b) running the code might take less time if sufficient compute resources were to be brought to bear, but no such resources can be accessed in a timely fashion (c) the replication package is very complex, and following all (manual and scripted) steps would take too long.
- [ ] `Data missing` is marked when data *should* be available, but was erroneously not provided, or is not accessible via the procedures described in the replication package
- [ ] `Data not available` is marked when data requires additional access steps, for instance purchase or application procedure. 
