
- [REQUIRED] Please provide a clear description of access modality and source location for this dataset. Some examples are given [on this website](https://social-science-data-editors.github.io/guidance/Requested_information_dcas.html).

- [REQUIRED] Please provide a mapping of programs to Table 10, 11, and 12, or add comments to the code that identify where Table 10, 11, and 12 is produced.

- [REQUIRED] Please provide complete code, including for appendix tables 10-12. 

- [REQUIRED] Please amend README to contain complete requirements. 

- [REQUIRED] Please add a setup program that installs all Stata packages. Please specify all necessary commands. An example of a setup file can be found at [https://github.com/gslab-econ/template/blob/master/config/config_stata.do](https://github.com/gslab-econ/template/blob/master/config/config_stata.do)

> The openICPSR submission process has changed. If you have not already done so, please "Change Status -> Submit to AEA" from your deposit Workspace.



Details in the full report, which you will receive via ScholarOne shortly. Please provide your response to the items listed above via the openICPSR Project Communication log, specifying AEAREP-2211. Other items in the report may need to be addressed via ScholarOne.
